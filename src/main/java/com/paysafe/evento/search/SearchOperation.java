package com.paysafe.evento.search;

public enum SearchOperation {
    DATE_FROM,
    DATE_TO,
    BETWEEN_DATES,
    EQUALS,
    BY_STATUSES,
    BY_CREATORS, 
    MY_EVENTS
}
