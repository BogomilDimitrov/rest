package com.paysafe.evento.search;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.entites.EventSubscriber;
import com.paysafe.evento.entites.SubscriptionType;
import com.paysafe.evento.entites.User;

/**
 * Uses {@link SearchCriteria} to create a search constraint and
 * construct the actual query as a Predicate.
 */
public class EventSpecification implements Specification<Event> {

    private SearchCriteria criteria;

    public EventSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(Root<Event> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        switch (criteria.getOperation()) {
            case DATE_FROM:
                return cb.greaterThanOrEqualTo(root.get("date"), criteria.getDate());
            case DATE_TO:
                return cb.lessThanOrEqualTo(root.get("date"), criteria.getDate());
            case BETWEEN_DATES:
                return cb.between(root.get("date"), criteria.getDateBefore(), criteria.getDateAfter());
            case EQUALS:
                if (root.get(criteria.getKey()).getJavaType() == String.class) {
                    return cb.like(root.get(criteria.getKey()), "%" + criteria.getValue() + "%");
                } else {
                    return cb.equal(root.get(criteria.getKey()), criteria.getValue());
                }
            case BY_STATUSES: {
                List<Predicate> predicates = new ArrayList<>();
                for (EventStatus eventStatus : criteria.getEventStatuses()) {
                    predicates.add(cb.equal(root.<String>get(criteria.getKey()), eventStatus));
                }
                return cb.or(predicates.toArray(new Predicate[]{}));
            }
            case BY_CREATORS: {
                List<Predicate> predicates = new ArrayList<>();
                for (User user : criteria.getCreators()) {
                    predicates.add(cb.equal(root.get("creator"), user));
                }
                return cb.or(predicates.toArray(new Predicate[]{}));
            } case MY_EVENTS: {
            	List<Predicate> predicates = new ArrayList<>();
    			
    			for (EventSubscriber es : ((User) criteria.getValue()).getSubscribedEvents()) {
    				if (es.getSubscriptionType() != SubscriptionType.NOT_GOING) {
    					predicates.add(cb.isMember(es, root.get("subscribedUsers")));
    				}
    			}
    			predicates.add(cb.equal(root.get("creator"), ((User) criteria.getValue())));
    			return cb.or(predicates.toArray(new Predicate[] {}));
    		}
        }
        return null;
    }
}
