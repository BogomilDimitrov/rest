package com.paysafe.evento.search;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.entites.User;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Builder class used for combining {@link SearchCriteria}
 * as one search query {@link Specification}.
 */
public class EventSpecificationBuilder {

    private List<SearchCriteria> params;

    public EventSpecificationBuilder() {
        this.params = new ArrayList<>();
    }

    public EventSpecificationBuilder with(String key, SearchOperation operation, Object value) {
        if (value != null && !value.toString().isEmpty()) {
            params.add(new SearchCriteria(key, operation, value));
        }
        return this;
    }
    
    
    public EventSpecificationBuilder withCurrent(String key, SearchOperation operation, User value) {
            params.add(new SearchCriteria(key, operation, value));
        
        return this;
    }

    public EventSpecificationBuilder inRangeOf(String key, SearchOperation operation, Date value) {
        if (value != null && !value.toString().isEmpty()) {
            params.add(new SearchCriteria(key, operation, value));
        }
        return this;
    }

    public EventSpecificationBuilder between(SearchOperation operation, Date dateBefore, Date dateAfter) {
        if (dateAfter != null && dateBefore != null
                && !dateAfter.toString().isEmpty()
                && !dateBefore.toString().isEmpty()) {
            params.add(new SearchCriteria(operation, dateBefore, dateAfter));
        }
        return this;
    }

    public EventSpecificationBuilder or(String key, SearchOperation operation, List<EventStatus> eventStatuses) {
        if (eventStatuses != null && !eventStatuses.isEmpty()) {
            params.add(new SearchCriteria(operation, key, eventStatuses));
        }
        return this;
    }

    public EventSpecificationBuilder withCreator(String key, SearchOperation operation, Collection<User> creators) {
        params.add(new SearchCriteria(key, operation, creators));
        return this;
    }

    public Specification<Event> build() {
        if (params.size() == 0) {
            return null;
        }

        Specification<Event> specification = null;
        for (SearchCriteria param : params) {
            specification = Specifications.where(specification).and(new EventSpecification(param));
        }

        return specification;
    }
}
