package com.paysafe.evento.search;

import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.entites.User;

import java.util.Collection;
import java.util.Date;

/**
 * Contains the specifics of an event search.
 */
public class SearchCriteria {
    private String key;
    private SearchOperation operation;
    private Object value;
    private Date dateBefore;
    private Date dateAfter;
    private Date date;
    private Collection<EventStatus> eventStatuses;
    private Collection<User> creators;

    /**
     * Constructor used for creating general search criteria.
     *
     * @param key the field name
     * @param operation the operation
     * @param value the field value
     */
    public SearchCriteria(String key, SearchOperation operation, Object value) {
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

    /**
     * Used for defining range of dates.
     */
    public SearchCriteria(SearchOperation operation, Date dateBefore, Date dateAfter) {
        this.operation = operation;
        this.dateBefore = dateBefore;
        this.dateAfter = dateAfter;
    }

    /**
     * Used for defining a criteria for after/before date search.
     */
    public SearchCriteria(String key, SearchOperation operation, Date date) {
    	this.key = key;
        this.operation = operation;
        this.date = date;
    }

    /**
     * Used for defining a search by a collection of statuses.
     */
    public SearchCriteria(SearchOperation operation, String key, Collection<EventStatus> eventStatuses) {
        this.operation = operation;
        this.key = key;
        this.eventStatuses = eventStatuses;
    }

    /**
     * Used for defining a search by a collection of users.
     */
    public SearchCriteria(String key, SearchOperation operation, Collection<User> creators ) {
        this.operation = operation;
        this.key = key;
        this.creators = creators;
    }

    public Collection<User> getCreators() {
		return this.creators;
	}

	public void setCreators(Collection<User> creators) {
		this.creators = creators;
	}

	public Date getDateBefore() {
        return dateBefore;
    }

    public void setDateBefore(Date dateBefore) {
        this.dateBefore = dateBefore;
    }

    public Date getDateAfter() {
        return dateAfter;
    }

    public void setDateAfter(Date dateAfter) {
        this.dateAfter = dateAfter;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Collection<EventStatus> getEventStatuses() {
        return eventStatuses;
    }

    public void setEventStatuses(Collection<EventStatus> eventStatuses) {
        this.eventStatuses = eventStatuses;
    }

    public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

    public SearchOperation getOperation() {
        return operation;
    }

    public void setOperation(SearchOperation operation) {
        this.operation = operation;
    }
}
