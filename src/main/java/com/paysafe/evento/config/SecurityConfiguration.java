package com.paysafe.evento.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;



@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Value("${evento.base.url}")
	private String logoutRedirectURI;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/users/authorize",
                        "/eventoInfoService",
                        "/users/isStillLogin",
                        "/events/limit/**",
                        "/v2/api-docs", "/swagger-ui.html",
                        "/webjars/springfox-swagger-ui/**",
                        "/swagger-resources/configuration/ui",
                        "/swagger-resources/**",
                        "/users/registerUser",
                        "/users/signIn",
                        "/users/getAll")
                .permitAll()
                .anyRequest().authenticated()
                .and().csrf().disable()
                .logout()
                .deleteCookies("JSESSIONID")
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl(this.logoutRedirectURI).permitAll();
    }
}

