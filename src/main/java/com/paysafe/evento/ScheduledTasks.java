package com.paysafe.evento;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.repositories.EventRepository;

@Component
public class ScheduledTasks {
	@Autowired
	private EventRepository eventRepository;

	/**
	 * Scheduled execution - daily at 1:01 AM Automatically close events
	 * according to their registration or due date.
	 */
	@Scheduled(cron = "0 1 1 * * *")
	public void closeEvents() {
		for (Event event : this.eventRepository.findAll()) {
			if (event.getDueDate().before(Timestamp.valueOf(LocalDateTime.now()))
					&& event.getStatus().equals(EventStatus.OPEN_FOR_ENROLLMENT)) {
				event.setStatus(EventStatus.CLOSED_FOR_ENROLLMENT);
			}
			if (event.getDate().before(Timestamp.valueOf(LocalDateTime.now()))
					&& event.getStatus().equals(EventStatus.CLOSED_FOR_ENROLLMENT)) {
				event.setStatus(EventStatus.CLOSED);
			}
			this.eventRepository.save(event);
		}

	}
}