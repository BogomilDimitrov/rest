package com.paysafe.evento;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;

@RestController
@RequestMapping(path = "/eventoInfoService")
class InfoService {
    @PersistenceContext
    private EntityManager em;

    @RequestMapping(method = RequestMethod.GET)
    public String test() {
        return this.getTimeFromDB();
    }

    private String getTimeFromDB() {
        Query query = em.createNativeQuery("SELECT NOW()");
        Timestamp timestamp = (Timestamp) query.getSingleResult();
        return timestamp.toString();
    }
}