package com.paysafe.evento.entites;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

/**
 * This entity is used for event categorization.
 * Each event is linked to one unique group and each group is linked to a number of events.
 * <p>The group name is unique and not null.</p>
 * {@link Event}
 */
@Entity
@Table(name = "groups")
public class Group extends BaseEntity {
    private String name;
    private Collection<Event> events;
    private Collection<UserGroupSubscription> userGroupSubscriptions;

    public Group() {
    }

    public Group(String name) {
        this.name = name;
        this.events = new HashSet<>();
    }

    @Column(unique = true)
    @NotNull
    @Length(min = 2, max = 40)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "assignedGroups")
    @JsonIgnore
    public Collection<Event> getEvents() {
        return events;
    }

    public void setEvents(Collection<Event> events) {
        this.events = events;
    }

    @JsonIgnore
    @OneToMany(targetEntity = UserMailSubscription.class, mappedBy = "mailTemplates",
            cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public Collection<UserGroupSubscription> getUserGroupSubscriptions() {
        return this.userGroupSubscriptions;
    }

    public void setUserGroupSubscriptions(Collection<UserGroupSubscription> userGroupSubscriptions) {
        this.userGroupSubscriptions = userGroupSubscriptions;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                '}';
    }
}
