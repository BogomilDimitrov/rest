package com.paysafe.evento.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;

/**
 * Entity Event
 * Stores information about an event in the application.
 * Stores Collection with the subscribed users that are provided by the EventSubscriber entity
 */
@Entity
@Table
public class Event extends BaseEntity {

	private String name;
    private String description;
    private String link;
    private String location;
    private User creator;
    private EventStatus status;
    private Timestamp date;
    private Timestamp dueDate;
    private Timestamp dateOfCreation;
    private Collection<Group> assignedGroups;
    private Collection<EventSubscriber> subscribedUsers;
    private Collection<StatusChangeEntry> history;
    private boolean havingPicture;

    public Event(){
        this.subscribedUsers = new HashSet<>();
        this.history = new HashSet<>();
        this.assignedGroups = new HashSet<>();
        this.havingPicture = false;
    }

    public Event(String name, Timestamp date, String description, String link, String location, EventStatus status, Timestamp dueDate, User creator, Timestamp dateOfCreation) {
        this();
        this.name = name;
        this.date = date;
        this.description = description;
        this.link = link;
        this.location = location;
        this.status = status;
        this.dueDate = dueDate;
        this.creator = creator;
        this.dateOfCreation = dateOfCreation;
    }

	@Column
    @NotNull
    @Length(min = 3, max = 61)

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column
    @NotNull
    public Timestamp getDate() {
        return this.date;
    }

    public void setDate(java.sql.Timestamp date) {
        this.date = date;
    }

    @Column
    @Length(max = 5000)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column
    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Column
    @NotNull
    @Length(min = 2, max = 254)
    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Column
    @Enumerated(EnumType.STRING)
    public EventStatus getStatus() {
        return this.status;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }

    @Column
    @NotNull
    public Timestamp getDueDate() {
        return this.dueDate;
    }

    public void setDueDate(Timestamp dueDate) {
        this.dueDate = dueDate;
    }

    @OneToMany(mappedBy = "event",
            targetEntity = EventSubscriber.class,
            cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnore
    public Collection<EventSubscriber> getSubscribedUsers() {
        return this.subscribedUsers;
    }

    public void setSubscribedUsers(Collection<EventSubscriber> subscribedUsers) {
        this.subscribedUsers = subscribedUsers;
    }

    @OneToMany(mappedBy="event",cascade={CascadeType.MERGE,CascadeType.PERSIST})
    @JsonIgnore
    public Collection<StatusChangeEntry> getHistory() {
		return this.history;
	}

	public void setHistory(Collection<StatusChangeEntry> history) {
		this.history = history;
	}

    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    @NotNull
    public User getCreator() {
        return this.creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    @Column
    public Timestamp getDateOfCreation() {
        return this.dateOfCreation;
    }

    public void setDateOfCreation(Timestamp dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "events_groups", joinColumns = {
			@JoinColumn(name = "event_id", referencedColumnName = "id" , nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "group_id", referencedColumnName = "id",
					nullable = false, updatable = false) })
    public Collection<Group> getAssignedGroups() {
        return this.assignedGroups;
    }

    public void setAssignedGroups(Collection<Group> assignedGroups) {
        this.assignedGroups = assignedGroups;
    }

    @Column(name = "has_picture")
    public boolean isHavingPicture() {
        return havingPicture;
    }

    public void setHavingPicture(boolean havingPicture) {
        this.havingPicture = havingPicture;
    }

    public void copyEvent(Event event) {
    	this.name = event.name;
    	this.date = event.date;
    	this.description = event.description;
    	this.link = event.link;
    	this.location = event.location;
    	this.dueDate = event.dueDate;
    	this.creator = event.creator;
    	this.assignedGroups = event.assignedGroups;
        event.havingPicture = false;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", date=" + date +
                ", description='" + description + '\'' +
                ", link='" + link + '\'' +
                ", location='" + location + '\'' +
                ", status=" + status +
                ", dueDate=" + dueDate +
                ", creator=" + creator +
                ", dateOfCreation=" + dateOfCreation +
                ", groups=" + assignedGroups +
                ", history=" + history +
                ", havingPicture" + havingPicture +
                '}';
    }
}