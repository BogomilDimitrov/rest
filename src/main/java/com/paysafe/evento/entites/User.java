package com.paysafe.evento.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Entity User
 * Stores information about Evento user.
 * Stores Collection with the events the user has subscribed to,
 * that are provided by the EventSubsciber entity.
 */
@Entity
public class User extends BaseEntity implements UserDetails {
    private String username;
    private String password;
    private String name;
    private String email;
    private String sub;
    private String pictureURL;
    private UserRole role;
    private boolean termsAccepted;
    private boolean enabledNotifications;
    private Collection<Event> createdEvents;
    private Collection<EventSubscriber> subscribedEvents;
    private Collection<StatusChangeEntry> eventsChangedByUser;
    @JsonIgnore
    private Collection<UserMailSubscription> userMailSubscriptions;
    @JsonIgnore
    private Collection<UserGroupSubscription> userGroupSubscriptions;

    public User() {
    	this.enabledNotifications = true;
    }
    
    @Column
    @NotNull
    @Length(min = 3, max = 151)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(unique = true)
    @NotNull
    @Length(min = 3, max = 151)
    @Email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(unique = true)
    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    @Column
    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    @Column
    @Enumerated(EnumType.STRING)
    @NotNull
    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Column
    @NotNull
    public boolean isTermsAccepted() {
        return termsAccepted;
    }

    public void setTermsAccepted(boolean termsAccepted) {
        this.termsAccepted = termsAccepted;
    }

    @Column(name = "enabled_notifications")
    @NotNull
    public boolean isEnabledNotifications() {
        return enabledNotifications;
    }

    public void setEnabledNotifications(boolean enabledNotifications) {
        this.enabledNotifications = enabledNotifications;
    }

    @OneToMany(mappedBy = "subscriber",
            cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnore
    public Collection<EventSubscriber> getSubscribedEvents() {
        return subscribedEvents;
    }

    public void setSubscribedEvents(Collection<EventSubscriber> subscribedEvents) {
        this.subscribedEvents = subscribedEvents;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "userId", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public Collection<StatusChangeEntry> getEventsChangedByUser() {
        return eventsChangedByUser;
    }

    public void setEventsChangedByUser(Collection<StatusChangeEntry> eventsChangedByUser) {
        this.eventsChangedByUser = eventsChangedByUser;
    }

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnore
    public Collection<Event> getCreatedEvents() {
        return createdEvents;
    }

    public void setCreatedEvents(Collection<Event> createdEvents) {
        this.createdEvents = createdEvents;
    }

    @OneToMany(targetEntity = UserMailSubscription.class, mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnore
    public Collection<UserMailSubscription> getUserMailSubscriptions() {
        return userMailSubscriptions;
    }

    public void setUserMailSubscriptions(Collection<UserMailSubscription> userMailSubscriptions) {
        this.userMailSubscriptions = userMailSubscriptions;
    }

    @OneToMany(targetEntity = UserGroupSubscription.class, mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonIgnore
    public Collection<UserGroupSubscription> getUserGroupSubscriptions() {
        return userGroupSubscriptions;
    }

    public void setUserGroupSubscriptions(Collection<UserGroupSubscription> userGroupSubscriptions) {
        this.userGroupSubscriptions = userGroupSubscriptions;
    }

    @Override
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    @Column(unique = true)
    @Length(min = 3, max = 151)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    @Transient
    public boolean isEnabled() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return email.equals(user.email);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role +
                ", enabledNotifications=" + enabledNotifications +
                ", subscribedEvents=" + subscribedEvents +
                ", eventsChangedByUser=" + eventsChangedByUser +
                ", createdEvents=" + createdEvents +
                '}';
    }
}