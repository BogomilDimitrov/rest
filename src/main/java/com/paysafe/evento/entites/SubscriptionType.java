package com.paysafe.evento.entites;

/**
 * Provides types of subscription status for event.
 */
public enum SubscriptionType {
    GOING, INTERESTED, NOT_GOING
}
