package com.paysafe.evento.entites;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.sql.Timestamp;

@Entity
@Table(name="status_history")
public class StatusChangeEntry extends BaseEntity {

    private Event event;
	private EventStatus status;	
	private java.sql.Timestamp changeDate;
	private User userId;

	public StatusChangeEntry() {
		this.changeDate = Timestamp.valueOf(LocalDateTime.now());
	}

	public StatusChangeEntry(Event event, EventStatus status, User userId) {
		this();
		this.event = event;
		this.status = status;
		this.userId = userId;
	}

	@Column
	@Enumerated(EnumType.STRING)
	public EventStatus getStatus() {
		return status;
	}

	public void setStatus(EventStatus status) {
		this.status = status;
	}

	@Column
	public java.sql.Timestamp getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(java.sql.Timestamp changeDate) {
		this.changeDate = changeDate;
	}
	
	@ManyToOne(cascade={CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "event_id", referencedColumnName = "id")
	public Event getEvent() {
		return event;
	}
	
	public void setEvent(Event event) {
		this.event = event;
	}
	
	@ManyToOne(cascade={CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "user_id", referencedColumnName = "id")
	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}
}
