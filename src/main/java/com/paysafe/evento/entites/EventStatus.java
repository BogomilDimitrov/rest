package com.paysafe.evento.entites;

/**
 * Provides types of status an event may have.
 */
public enum EventStatus {
    OPEN_FOR_ENROLLMENT, CLOSED, CANCELLED, CLOSED_FOR_ENROLLMENT
}