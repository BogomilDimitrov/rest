package com.paysafe.evento.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

/**
 * Entity EventSubscriber
 * Join table that links a particular user subscription to an event and
 * contains the subscription type (GOING,INTERESTED)
 */
@Entity
@Table(name = "event_subscriber")
public class EventSubscriber extends BaseEntity {
    private Event event;
    private User subscriber;
    private SubscriptionType subscriptionType;
    private String feedbackComment;
    private int feedbackRating;

    @JsonIgnore
    @ManyToOne(targetEntity = Event.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "event_id", referencedColumnName = "id")
    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "subscriber_id", referencedColumnName = "id")
    public User getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(User subscriber) {
        this.subscriber = subscriber;
    }

    @Column(name = "subscription_type")
    @Enumerated(EnumType.STRING)
    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    @Column
    @Length(max = 800)
    public String getFeedbackComment() {
        return feedbackComment;
    }

    public void setFeedbackComment(String feedbackComment) {
        this.feedbackComment = feedbackComment;
    }

    @Column
    public int getFeedbackRating() {
        return feedbackRating;
    }

    public void setFeedbackRating(int feedbackRating) {
        this.feedbackRating = feedbackRating;
    }
}
