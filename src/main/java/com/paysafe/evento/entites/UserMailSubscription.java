package com.paysafe.evento.entites;

import javax.persistence.*;

@Entity
public class UserMailSubscription extends BaseEntity {
    private User user;
    private MailTemplates mailTemplates;
    private boolean isEnabled;

    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "subscriber_id", referencedColumnName = "id")
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne(targetEntity = MailTemplates.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "template_id", referencedColumnName = "id")
    public MailTemplates getMailTemplates() {
        return this.mailTemplates;
    }

    public void setMailTemplates(MailTemplates mailTemplates) {
        this.mailTemplates = mailTemplates;
    }

    @Column(name = "isEnabled")
    public boolean isEnabled() {
        return this.isEnabled;
    }

    public void setEnabled(boolean enabled) {
        this.isEnabled = enabled;
    }
}
