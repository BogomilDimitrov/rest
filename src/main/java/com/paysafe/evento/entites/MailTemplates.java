package com.paysafe.evento.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Entity MailTemplates
 * Stores information that will be used by the mail service.
 * Different type of mail template will be used according to the status (NEW,REOPENED,CANCELED etc.)
 */
@Entity
@Table(name="mail_templates")
public class MailTemplates extends BaseEntity {
	private String subject;
	private String filePath;
	private String type;
    private Collection<UserMailSubscription> userMailSubscriptions;

    public MailTemplates(){

    }

	@Column
	@NotNull
	@Length(min = 3, max = 254)
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name="file_path")
	@NotNull
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Column(name="event_type", unique = true)
	@NotNull
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonIgnore
	@OneToMany(targetEntity = UserMailSubscription.class, mappedBy = "mailTemplates",
            cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    public Collection<UserMailSubscription> getUserMailSubscriptions() {
        return userMailSubscriptions;
    }

    public void setUserMailSubscriptions(Collection<UserMailSubscription> userMailSubscriptions) {
        this.userMailSubscriptions = userMailSubscriptions;
    }
}
