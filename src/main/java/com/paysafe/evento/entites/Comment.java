package com.paysafe.evento.entites;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

/**
 *This entity is used for saving comments for every event.
 * <p>
 *     Every comment has only one creator {@see User} and
 *     it is linked to one event{@see Event}. The comment contains
 *     date of creation {@see Timestamp} and text {@see String}.
 * </p>
 *
 * <p>
 *     Each event has many comments and fne Event may
 *     have many comments with same creator.
 * </p>
 */
@Entity
public class Comment extends BaseEntity {

    private String content;
    private Timestamp dateOfCreation;
    private User creator;
    private Event event;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Timestamp dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    @ManyToOne(targetEntity = User.class, cascade = CascadeType.ALL)
    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    @ManyToOne(targetEntity = Event.class, cascade = CascadeType.ALL)
    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

	@Override
	public String toString() {
		return "Comment [content=" + content + ", dateOfCreation=" + dateOfCreation + ", creator=" + creator + "]";
	}
    
}
