package com.paysafe.evento.entites;

import javax.persistence.*;
import java.io.Serializable;

@Entity

public class UserGroupSubscription extends BaseEntity implements Serializable {
    private User user;
    private Group subscribedGroup;
    private boolean isNotified;
    private boolean isSubscribed;

    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "subscriber_id", referencedColumnName = "id")
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne(targetEntity = Group.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    public Group getSubscribedGroup() {
        return this.subscribedGroup;
    }

    public void setSubscribedGroup(Group subscribedGroup) {
        this.subscribedGroup = subscribedGroup;
    }

    @Column(name = "is_notified")
    public boolean isNotified() {
        return this.isNotified;
    }

    public void setNotified(boolean isNotified) {
        this.isNotified = isNotified;
    }

    @Column(name = "is_subscribed")
    public boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(boolean subscribed) {
        isSubscribed = subscribed;
    }
}
