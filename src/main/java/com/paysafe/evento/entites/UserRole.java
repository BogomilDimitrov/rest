package com.paysafe.evento.entites;

/**
 * Provides types of roles for users.
 */
public enum UserRole {
    USER, ADMIN
}
