package com.paysafe.evento.repositories;

import com.paysafe.evento.entites.User;
import com.paysafe.evento.entites.UserGroupSubscription;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface UserGroupSubscriptionRepository extends PagingAndSortingRepository<UserGroupSubscription, String> {
    Collection<UserGroupSubscription> findAllByUser(User user);

    Collection<UserGroupSubscription> findAllByUserAndSubscribed(User user, boolean isSubscribed);

    Collection<UserGroupSubscription> findAllByUserAndNotifiedAndSubscribed(User user, boolean notified, boolean subscribed);

}
