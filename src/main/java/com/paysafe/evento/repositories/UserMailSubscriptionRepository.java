package com.paysafe.evento.repositories;

import com.paysafe.evento.entites.MailTemplates;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.entites.UserMailSubscription;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMailSubscriptionRepository extends PagingAndSortingRepository<UserMailSubscription, String> {
    UserMailSubscription findFirstByMailTemplatesAndUser(MailTemplates mailTemplates, User user);

    List<UserMailSubscription> findAllByUser(User user);
}
