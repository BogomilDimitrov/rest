package com.paysafe.evento.repositories;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventSubscriber;
import com.paysafe.evento.entites.SubscriptionType;
import com.paysafe.evento.entites.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface EventSubscriberRepository extends PagingAndSortingRepository<EventSubscriber, String> {
    EventSubscriber findEventSubscriberBySubscriberAndEvent(User subscriber, Event event);

    Collection<EventSubscriber> findEventSubscribersByEventAndSubscriptionTypeIn(Event event, List<SubscriptionType> subscriptionTypes);

    Collection<EventSubscriber> findEventSubscribersByEvent(Event event);

    Collection<EventSubscriber> findEventSubscribersByEventId(String eventId);

    List<EventSubscriber> findByEventAndFeedbackCommentIsNotNullAndFeedbackRatingIsNotNull(Event event);

    Integer countAllBySubscriptionTypeAndEvent(SubscriptionType subscriptionType, Event event);

    Collection<EventSubscriber> findEventSubscribersBySubscriber(User subscriber);

    Collection<EventSubscriber> findAllBySubscriberAndSubscriptionTypeIn(User subscriber, List<SubscriptionType> subscriptionTypes);

    Collection<EventSubscriber> findEventSubscriberBySubscriberAndSubscriptionTypeIn(User user, List<SubscriptionType> types);
}
