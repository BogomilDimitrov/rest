package com.paysafe.evento.repositories;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.entites.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface EventRepository extends PagingAndSortingRepository<Event, String> {

    Collection<Event> findAll();

    Page<Event> findAll(Specification spec, Pageable pageable);

    @Query("select e from Event e WHERE e.status in :statuses")
    Collection<Event> findEventsByStatusOrderByDate(@Param("statuses") List<EventStatus> statuses);

    Event findById(String id);

    List<Event> findEventsByStatusOrderByDate(EventStatus status, Pageable pageable);

    List<Event> findEventsByCreatorAndStatusIn(User user, List<EventStatus> eventStatuses);


}
