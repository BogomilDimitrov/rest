package com.paysafe.evento.repositories;

import com.paysafe.evento.entites.Comment;
import com.paysafe.evento.entites.Event;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface CommentRepository extends PagingAndSortingRepository<Comment, String> {

    Collection<Comment> findAllByEventOrderByDateOfCreationDesc(Event event);
}
