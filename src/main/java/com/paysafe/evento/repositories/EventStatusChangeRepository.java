package com.paysafe.evento.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.paysafe.evento.entites.StatusChangeEntry;
import org.springframework.stereotype.Repository;

@Repository
public interface EventStatusChangeRepository extends PagingAndSortingRepository<StatusChangeEntry, String> {

}
