package com.paysafe.evento.repositories;

import com.paysafe.evento.entites.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Component
@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {

    User findUserBySub(String sub);

    User findUserById(String id);

    User findUserByUsername(String username);

    Collection<User> findAllByNameContainingIgnoreCase(String name);

}
