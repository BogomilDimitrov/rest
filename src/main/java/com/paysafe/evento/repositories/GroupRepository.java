package com.paysafe.evento.repositories;

import com.paysafe.evento.entites.Group;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends PagingAndSortingRepository<Group, String> {

    Group findByName(String name); 
}