package com.paysafe.evento.repositories;

import com.paysafe.evento.entites.MailTemplates;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface MailTemplatesRepository extends PagingAndSortingRepository<MailTemplates, String> {

    MailTemplates findByType(String type);

    Collection<MailTemplates> findAll();
}
