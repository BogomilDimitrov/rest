package com.paysafe.evento.controllers;

import com.paysafe.evento.entites.*;
import com.paysafe.evento.report.Template;
import com.paysafe.evento.repositories.EventRepository;
import com.paysafe.evento.repositories.EventSubscriberRepository;
import com.paysafe.evento.repositories.UserRepository;
import net.sf.dynamicreports.jasper.builder.JasperConcatenatedReportBuilder;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.jasper.builder.export.JasperXlsxExporterBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.column.Columns;
import net.sf.dynamicreports.report.builder.component.Components;
import net.sf.dynamicreports.report.builder.datatype.DataTypes;
import net.sf.dynamicreports.report.definition.datatype.DRIDataType;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

@RestController
@RequestMapping(value = "/reports")
@CrossOrigin("*")
public class ReportController {
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private EventSubscriberRepository eventSubscriberRepository;
    @Autowired
    private UserRepository userRepository;

    private ByteArrayOutputStream reportOutputStream;

    @Value("${evento.base.url")
    private String eventoBaseUrl;
    private String eventsUrl = "http://evento.neterra.skrill.net/events/";

    /**
     * Provides a generated report to the caller.
     * <p>
     * Report document body is converted to byte array stream.
     * {@link MediaType#APPLICATION_OCTET_STREAM}.
     *
     * @param id unique Event id taken from the front end.
     * @return HttpEntity containing byte array
     * @throws SQLException when an error occurs
     */
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public HttpEntity<byte[]> exportReportInExcelFile(@PathVariable(name = "id") String id) throws SQLException {
        Event event = this.eventRepository.findOne(id);
        createReport(event);

        byte[] documentBody = this.reportOutputStream.toByteArray();

        String fileName = event.getName();
        try {
            fileName = URLEncoder.encode(event.getName(), "UTF-8");
            fileName = URLDecoder.decode(fileName, "ISO8859_1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=Report for " + fileName + " Event.xlsx");
        header.setContentLength(documentBody.length);
        return new HttpEntity<>(documentBody, header);
    }

    @RequestMapping(path = "/user/{id}")
    public HttpEntity<byte[]> exportUserReportToExcelFile(@PathVariable(name = "id") String id) throws SQLException {
        User user = this.userRepository.findOne(id);
        createUserReport(user);
        byte[] documentBody = this.reportOutputStream.toByteArray();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=Report for " + user.getName() + " User.xlsx");
        header.setContentLength(documentBody.length);
        return new HttpEntity<>(documentBody, header);
    }

    @RequestMapping(path = "/statistics")
    public HttpEntity<byte[]> exportGenerateStatistics() throws SQLException {
        generateStatisticsReport();
        byte[] documentBody = this.reportOutputStream.toByteArray();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=Evento Report.xlsx");
        header.setContentLength(documentBody.length);
        return new HttpEntity<>(documentBody, header);

    }

    private void generateStatisticsReport() {
        List<CurrentStatistic> statistics = new ArrayList<>();
        List<EventStatus> statuses = new ArrayList<>();
        statuses.add(EventStatus.OPEN_FOR_ENROLLMENT);
        statuses.add(EventStatus.CLOSED_FOR_ENROLLMENT);
        Collection<Event> nonClosedEvents = eventRepository.findEventsByStatusOrderByDate(statuses);

        for (Event event : nonClosedEvents) {
            Collection<EventSubscriber> subscribers = this.eventSubscriberRepository.findEventSubscribersByEvent(event);
            for (EventSubscriber es : subscribers) {
                if (!es.getSubscriptionType().toString().equalsIgnoreCase("NOT_GOING")) {
                    CurrentStatistic cs = new CurrentStatistic();
                    cs.setDate(event.getDate().toString());
                    cs.setEventName(event.getName());
                    cs.setUserName(es.getSubscriber().getName());
                    cs.setSubscriptionType(es.getSubscriptionType().toString());
                    cs.setEventLink(this.eventsUrl + es.getEvent().getId());
                    statistics.add(cs);
                }

            }
        }

        JasperReportBuilder statisticsRepott = getJasperReportBuilder("Evento Statistics",
                statistics,
                new String[]{"Date", "Event name", "User name", "Subscription Type", "Event link"},
                new String[]{"date", "eventName", "userName", "subscriptionType", "eventLink"},
                DataTypes.stringType());
        this.reportOutputStream = new ByteArrayOutputStream();
        try {
            JasperXlsxExporterBuilder xlsExporter = DynamicReports.export
                    .xlsxExporter(this.reportOutputStream)
                    .setRemoveEmptySpaceBetweenColumns(true)
                    .setRemoveEmptySpaceBetweenRows(true);
            statisticsRepott.toXlsx(xlsExporter);
        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates two reports and concatenates them into one.
     * <p>
     * The first report consists of all User Names that are registered as going or interested.
     * The second report contains the count of all users separated in the same two categories.
     *
     * @param event Event object to get report for
     * @throws SQLException when an error occurs
     */
    private void createReport(Event event) throws SQLException {

        List<SubscribedUser> subscribedUserList = getSubscribedUsers(event);

        List<SubscriptionCounters> countersList = getSubscriptionCounters(event);

        JasperConcatenatedReportBuilder report = DynamicReports.concatenatedReport();
        report.continuousPageNumbering();
        JasperReportBuilder subscribedUsersReport = getJasperReportBuilder(event.getName(),
                subscribedUserList,
                new String[]{"Name", "Email", "Subscription Type"},
                new String[]{"name", "email", "subscription_type"},
                DataTypes.stringType());
        JasperReportBuilder totalSubscriptionsReport = getJasperReportBuilder(event.getName(),
                countersList,
                new String[]{"Going", "Interested", "Total"},
                new String[]{"going", "interested", "total"},
                DataTypes.integerType());
        report.concatenate(subscribedUsersReport);
        report.concatenate(totalSubscriptionsReport);
        report.setContinuousPageNumbering(true);
        this.reportOutputStream = new ByteArrayOutputStream();
        try {
            JasperXlsxExporterBuilder xlsExporter = DynamicReports.export
                    .xlsxExporter(this.reportOutputStream)
                    .setRemoveEmptySpaceBetweenColumns(true)
                    .setRemoveEmptySpaceBetweenRows(true);
            report.toXlsx(xlsExporter);
        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    private void createUserReport(User user) {
        List<UserSubscriber> subscribers = getUserSubscribers(user);

        JasperReportBuilder reportBuilder = getJasperReportBuilder(user.getName(),
                subscribers,
                new String[]{"Date", "Name", "Subscription Type", "Event Link"},
                new String[]{"date", "eventName", "subscriptionType", "eventLink"},
                DataTypes.stringType());
        this.reportOutputStream = new ByteArrayOutputStream();
        try {
            JasperXlsxExporterBuilder xlsExporter = DynamicReports.export
                    .xlsxExporter(this.reportOutputStream)
                    .setRemoveEmptySpaceBetweenColumns(true)
                    .setRemoveEmptySpaceBetweenRows(true)
                    .setIgnorePageMargins(true);
            reportBuilder.toXlsx(xlsExporter);
        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    private List<UserSubscriber> getUserSubscribers(User user) {
        List<UserSubscriber> subscribers = new ArrayList<>();
        Collection<EventSubscriber> eventSubscribers = this.eventSubscriberRepository.findEventSubscribersBySubscriber(user);
        for (EventSubscriber es : eventSubscribers) {
            if (!es.getSubscriptionType().toString().equalsIgnoreCase("NOT_GOING")) {
                UserSubscriber us = new UserSubscriber();
                us.setDate(es.getEvent().getDate().toString());
                us.setEventName(es.getEvent().getName());
                us.setSubscriptionType(es.getSubscriptionType().toString());
                us.setEventLink(this.eventsUrl + es.getEvent().getId());
                subscribers.add(us);
            }
        }
        return subscribers;
    }

    /**
     * Retrieves list of Subscription Counters (helper object) which holds subscriptions going, interested and
     * total numbers for a given Event
     *
     * @param event Event object to list its subscriber metrics
     * @return List of Subscription Counters (expected only 1 holding all values).
     */
    private List<SubscriptionCounters> getSubscriptionCounters(Event event) {
        SubscriptionCounters subscriptionCounters = new SubscriptionCounters();
        subscriptionCounters.setGoing(this.eventSubscriberRepository
                .countAllBySubscriptionTypeAndEvent(SubscriptionType.GOING, event));
        subscriptionCounters.setInterested(this.eventSubscriberRepository
                .countAllBySubscriptionTypeAndEvent(SubscriptionType.INTERESTED, event));
        subscriptionCounters.setTotal(subscriptionCounters.getGoing() + subscriptionCounters.getInterested());
        List<SubscriptionCounters> countersList = new ArrayList<>();
        countersList.add(subscriptionCounters);
        return countersList;
    }

    /**
     * Method for populating List of SubscibedUser objects (helper class).
     * Gets all the subscribed users for a given event, holding specific data
     * - user name, user email and subscription type.
     *
     * @param event Event object to list its subscribed users
     * @return List of SubscribedUser (helper class)
     */
    private List<SubscribedUser> getSubscribedUsers(Event event) {
        List<SubscribedUser> subscribedUserList = new ArrayList<>();
        for (EventSubscriber es : event.getSubscribedUsers()) {
            if (es.getSubscriptionType() != SubscriptionType.NOT_GOING) {
                SubscribedUser user = new SubscribedUser();
                user.setName(es.getSubscriber().getName());
                user.setEmail(es.getSubscriber().getEmail());
                user.setSubscription_type(es.getSubscriptionType().toString());
                subscribedUserList.add(user);
            }
        }
        return subscribedUserList;
    }

    /**
     * Creates report of three columns for a given event, taking collection of data source (real or helper class),
     * 3 coulmn titles ad 3 field names (need to be exactly the same as the ones in the data source class) and data type
     * (String, Integer, etc).
     *
     * @param eventName    String name of the event reported
     * @param dataSource   Collection of data source classes (their data is used for display at each row)
     * @param columnTitles array of column titles (expected amount 3)
     * @param fieldNames   the field names to be used as data in the columns (exactly from the data source)
     * @param columnsType  DataTypes value ( for string - .stringType())
     * @return JasperReportBuilder Report data to be used in the concatenated report.
     */
    private JasperReportBuilder getJasperReportBuilder(String eventName, Collection<?> dataSource, String[] columnTitles, String[] fieldNames, DRIDataType columnsType) {
        JasperReportBuilder report = DynamicReports.report();
        JRDataSource dataSourceSubscribers = new JRBeanCollectionDataSource(dataSource);
        IntStream.range(0, columnTitles.length)
                .forEach((i) -> report.addColumn(Columns.column(columnTitles[i], fieldNames[i], columnsType)));
        report
                .title(
                        Template.createTitleComponent(eventName))
                .pageFooter(Components.pageXofY())
                .ignorePagination()
                .setDataSource(dataSourceSubscribers);
        return report;
    }

    /**
     * Helper class for keeping "going", "interested" and "total" number values for the subscribers of
     * event for the report.
     */
    public class SubscriptionCounters {
        private Integer going;
        private Integer interested;
        private Integer total;

        public Integer getGoing() {
            return going;
        }

        public void setGoing(Integer going) {
            this.going = going;
        }

        public Integer getInterested() {
            return interested;
        }

        public void setInterested(Integer interested) {
            this.interested = interested;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        @Override
        public String toString() {
            return "SubscriptionCounters{" +
                    "going=" + going +
                    ", interested=" + interested +
                    ", total=" + total +
                    '}';
        }
    }

    /**
     * Helper class for keeping subscriber's name, email and subscription type who are subscribed ro
     * the event reported.
     */
    public class SubscribedUser {
        private String name;
        private String email;
        private String subscription_type;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSubscription_type() {
            return subscription_type;
        }

        public void setSubscription_type(String subscription_type) {
            this.subscription_type = subscription_type;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }

    public class UserSubscriber {
        private String date;
        private String eventName;
        private String subscriptionType;
        private String eventLink;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getSubscriptionType() {
            return subscriptionType;
        }

        public void setSubscriptionType(String subscriptionType) {
            this.subscriptionType = subscriptionType;
        }

        public String getEventLink() {
            return eventLink;
        }

        public void setEventLink(String eventLink) {
            this.eventLink = eventLink;
        }
    }

    public class CurrentStatistic {
        private String date;
        private String eventName;
        private String userName;
        private String subscriptionType;
        private String eventLink;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getSubscriptionType() {
            return subscriptionType;
        }

        public void setSubscriptionType(String subscriptionType) {
            this.subscriptionType = subscriptionType;
        }

        public String getEventLink() {
            return eventLink;
        }

        public void setEventLink(String eventLink) {
            this.eventLink = eventLink;
        }
    }
}
