package com.paysafe.evento.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paysafe.evento.entites.*;
import com.paysafe.evento.mail.EmailServiceImpl;
import com.paysafe.evento.repositories.*;
import com.paysafe.evento.search.EventSpecificationBuilder;
import com.paysafe.evento.search.SearchOperation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/events")
@CrossOrigin("*")
public class EventController {

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private EmailServiceImpl emailServiceImpl;
    @Autowired
    private EventStatusChangeRepository eventStatusChangeRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EventSubscriberRepository eventSubscriberRepository;
    @Autowired
    private UserGroupSubscriptionRepository userGroupSubscriptionRepository;


    /**
     * Gets all events from the event repository and calls
     * {@link #eventWithSubscriptionTypeToJson} to get the current user's
     * subscriptions to the events.
     *
     * @return Collection of maps where each map consist of the subscription type as the key
     * and the event as the value.
     */
    @ApiOperation(value = "Get all events.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved all events."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public Collection<Map<String, Object>> getAllEvents(@AuthenticationPrincipal User currentUser) {
        Collection<Event> result = this.eventRepository.findAll();
        List<Map<String, Object>> resultList = new ArrayList<>();
        for (Event event : result) {
            this.eventWithSubscriptionTypeToJson(event, resultList, currentUser);
        }
        return resultList;
    }

    /**
     * Uses {@link EventSpecificationBuilder} to unite the passed search
     * parameters into one query.
     * <p>
     * Retrieves the events that associate with the current user in the following ways:
     * - events created by the user
     * - events that the user has subscribed to as GOING or INTERESTED.
     * The result events have status either OPEN_FOR_ENROLLMENT or
     * CLOSED_FOR_ENROLLMENT.
     * <p>
     *
     * @return Collection of maps where each map consist of the subscription type as the key
     * and the event as the value.
     */
    @ApiOperation(value = "Get current user's events.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved events."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/myEvents", method = RequestMethod.GET)
    public Collection<Map<String, Object>> getMyEvents(@AuthenticationPrincipal User currentUser) {
        Collection<Event> eventsByCreator = this.eventRepository.findEventsByCreatorAndStatusIn(currentUser,
                Arrays.asList(EventStatus.OPEN_FOR_ENROLLMENT, EventStatus.CLOSED_FOR_ENROLLMENT));
        Collection<EventSubscriber> subscribersByGoingAndInterested = this.eventSubscriberRepository
                .findEventSubscriberBySubscriberAndSubscriptionTypeIn(currentUser,
                        Arrays.asList(SubscriptionType.GOING, SubscriptionType.INTERESTED));

        Collection<Event> result = new ArrayList<>();
        result.addAll(eventsByCreator);
        for (EventSubscriber subscriber : subscribersByGoingAndInterested) {
            result.add(subscriber.getEvent());
        }

        result = result.stream().filter((event) ->
                event.getStatus().equals(EventStatus.OPEN_FOR_ENROLLMENT) ||
                        event.getStatus().equals(EventStatus.CLOSED_FOR_ENROLLMENT))
                .collect(Collectors.toSet());

        List<Map<String, Object>> resultList = new ArrayList<>();
        for (Event myEvent : result) {
            this.eventWithSubscriptionTypeToJson(myEvent, resultList, currentUser);
        }
        return resultList;
    }


    @ApiOperation(value = "Get event by ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved event."),
            @ApiResponse(code = 404, message = "Event not found."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/{eventId}", method = RequestMethod.GET)
    public ResponseEntity<Event> getEventById(@PathVariable("eventId") String eventId) {
        Event event = this.eventRepository.findById(eventId);
        if (event == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(event);
    }

    /**
     * This method retrieves events that cover the following criteria:
     * - events' statuses are either OPEN or CLOSED_FOR_ENROLLMENT;
     * - the current user is not the creator of any of them;
     * - the current user has not subscribed to any of them;
     * - events that are not included in any fo the groups that user has subscribed to.
     *
     * @return a collection of Events
     */
    @ApiOperation(value = "Get all events which is not my or in my groups and " +
            "with status OPEN_FOR_ENROLLMENT and CLOSED_FOR_ENROLLMENT.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved all events with status OPEN and CLOSED_FOR_ENROLLMENT."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(method = RequestMethod.GET)
    public Collection<Event> getEventsThatAreNotMineOrInMyGroups(@AuthenticationPrincipal User currentUser) {
        List<EventStatus> statuses = new ArrayList<>();
        statuses.add(EventStatus.OPEN_FOR_ENROLLMENT);
        statuses.add(EventStatus.CLOSED_FOR_ENROLLMENT);
        Collection<Event> events = this.eventRepository.findEventsByStatusOrderByDate(statuses);
        Collection<EventSubscriber> subscribersByGoingAndInterested = this.eventSubscriberRepository
                .findAllBySubscriberAndSubscriptionTypeIn(currentUser,
                        Arrays.asList(SubscriptionType.GOING, SubscriptionType.INTERESTED));
        Collection<UserGroupSubscription> subscriptions = this.userGroupSubscriptionRepository
                .findAllByUserAndSubscribed(currentUser, true);

        Collection<Event> subscribedEvents = new HashSet<>();
        for (EventSubscriber subscriber : subscribersByGoingAndInterested) {
            subscribedEvents.add(subscriber.getEvent());
        }

        Collection<Event> eventsInMyGroups = new HashSet<>();
        for (UserGroupSubscription subscription : subscriptions) {
            eventsInMyGroups.addAll(subscription.getSubscribedGroup().getEvents());
        }
        events = events.stream().filter((event) ->
                !event.getCreator().equals(currentUser)
                        && !eventsInMyGroups.contains(event)
                        && !subscribedEvents.contains(event)
        ).collect(Collectors.toSet());

        return events;
    }


    /**
     * Saves new event with default status OPEN_FOR_ENROLLMENT and sends email to the users.
     * Verifies the event date, the event due date and calls {@link #checkNonExistingGroup(Collection)}.
     *
     * @param event       contains all the necessary data for one {@link Event}
     * @param currentUser the current user which is set as the creator of the new event.
     * @return Response entity with the http status and an appropriate message.
     * If the event has been saved successfully, the ResponseEntity contains the event as
     * the body.
     */
    @ApiOperation(value = "Add new event.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added event."),
            @ApiResponse(code = 400, message = "Group does not exist."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity saveEvent(@RequestBody Event event, @AuthenticationPrincipal User currentUser) {
        if (event.getDate().before(new Timestamp(System.currentTimeMillis() - (1000 * 60 * 60 * 3)))) {
            return ResponseEntity.badRequest().body("Date should not be in the past.");
        }
        if (event.getDueDate().before(new Timestamp(System.currentTimeMillis() - (1000 * 60 * 60 * 3)))) {
            return ResponseEntity.badRequest().body("Enrollment deadline should not be in the past.");
        }

        if (event.getDueDate().after(event.getDate())) {
            return ResponseEntity.badRequest().body("Enrollment deadline should be before event date.");
        }
        event.setStatus(EventStatus.OPEN_FOR_ENROLLMENT);
        event.setCreator(this.userRepository.findUserById(currentUser.getId()));
        event.setDateOfCreation(Timestamp.valueOf(LocalDateTime.now()));
        Collection<Group> eventGroups = event.getAssignedGroups();
        boolean containsNonExistingGroup = this.checkNonExistingGroup(eventGroups);
        if (containsNonExistingGroup) {
            return ResponseEntity.badRequest().body("Group doesn't exist");
        }
        event.setAssignedGroups(eventGroups);
        this.eventRepository.save(event);
        this.emailServiceImpl.sendMailToAllUsers("NEW", event);
        return ResponseEntity.ok(event);
    }

    /**
     * Changes the event details except for the status.
     * Makes a copy of the edited event and saves it.
     * <p>
     * Sends email to all users that are subscribed and have enabled notifications.
     *
     * @param editedEvent - The edited event
     */
    @ApiOperation(value = "Edit event.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added event."),
            @ApiResponse(code = 400, message = "Group does not exist."),
            @ApiResponse(code = 404, message = "Event not found."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/edit", method = RequestMethod.PATCH)
    public ResponseEntity editEvent(@RequestBody Event editedEvent, @AuthenticationPrincipal User currentUser) {
        if (editedEvent.getDueDate().after(editedEvent.getDate())) {
            return ResponseEntity.badRequest().body("Enrollment deadline should be before event date.");
        }
        Collection<Group> eventGroups = editedEvent.getAssignedGroups();
        Event eventToBeChanged = this.eventRepository.findOne(editedEvent.getId());

        boolean containsNonExistingGroup = checkNonExistingGroup(eventGroups);
        if (containsNonExistingGroup) {
            return ResponseEntity.badRequest().body("Group doesn't exist");
        }
        User user = this.userRepository.findUserById(currentUser.getId());
        editedEvent.setCreator(user);
        if (eventToBeChanged == null) {
            return ResponseEntity.notFound().build();
        }
        if (!user.equals(eventToBeChanged.getCreator()) && user.getRole() != UserRole.ADMIN) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("You are not administrator or creator of this event ");
        }
        editedEvent.setAssignedGroups(eventGroups);
        eventToBeChanged.copyEvent(editedEvent);
        eventToBeChanged = this.eventRepository.save(eventToBeChanged);
        this.emailServiceImpl.sendMailToSubscribedUsers("CHANGED", eventToBeChanged);
        return ResponseEntity.ok(eventToBeChanged);
    }

    /**
     * Changes the status of a given event and saves the information
     * about user who has changed the event, the time of the change and
     * the new status {@link StatusChangeEntry}.
     *
     * @param idAndStatus a map containing the event id as a key and the
     *                    new status as a value.
     * @return response ok if event status is changed successfully,
     * or bad request response if the event or the status are not present.
     */
    @ApiOperation(value = "Change event status.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully changed event status."),
            @ApiResponse(code = 400, message = "Event/User/Status does not exist."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/changeStatus", method = RequestMethod.PATCH)
    public ResponseEntity<Event> changeStatus(@RequestBody Map<String, String> idAndStatus,
                                              @AuthenticationPrincipal User currentUser) {
        Event event = this.eventRepository.findOne(idAndStatus.get("eventId"));
        User user = this.userRepository.findUserById(currentUser.getId());
        Timestamp changeTime = Timestamp.valueOf(LocalDateTime.now());
        EventStatus eventStatus = EventStatus.valueOf(idAndStatus.get("statusType").toUpperCase());
        boolean statusExists = false;
        for (EventStatus status : EventStatus.values()) {
            if (status == eventStatus) {
                statusExists = true;
                break;
            }
        }
        if (event == null || !statusExists || user == null) {
            return ResponseEntity.badRequest().build();
        }

        if (!user.equals(event.getCreator()) && user.getRole() != UserRole.ADMIN) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        StatusChangeEntry currentStatusChange = new StatusChangeEntry();
        currentStatusChange.setEvent(event);
        currentStatusChange.setStatus(eventStatus);
        currentStatusChange.setChangeDate(changeTime);
        currentStatusChange.setUserId(user);
        this.eventStatusChangeRepository.save(currentStatusChange);
        event.setStatus(eventStatus);
        this.eventRepository.save(event);
        if (eventStatus == EventStatus.CLOSED) {
            this.emailServiceImpl.sendMailToSubscribedUsers("FEEDBACK", event);
        } else {
            this.emailServiceImpl.sendMailToSubscribedUsers("CHANGED", event);
        }
        return ResponseEntity.ok(event);
    }


    /**
     * Uses {@link EventSpecificationBuilder} to unite the passed search parameters
     * into one query.
     * The method checks if the "eventName", "dateFrom", "dateTo",
     * "creatorName" and "pageNumber" keys are present in the map and uses their values
     * to search by.
     * <p>
     * If any statuses are being delivered in the input, they will be processed
     * as a key in the map where the value is not necessary.
     * The method searches for keys with the string values of the enum Status
     * in lower case e.g. "open", "closed".
     *
     * @param searchParameters map containing the parameters and their values.
     * @return Page of events matching the parameters.
     */
    @ApiOperation(value = "Creates a query from passed search parameters.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retreived query."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public Page<Event> search(@RequestParam Map<String, String> searchParameters) {
        String eventName = null;
        Date dateFrom = null;
        Date dateTo = null;

        if (searchParameters.containsKey("eventName") && !searchParameters.get("eventName").isEmpty()) {
            eventName = searchParameters.get("eventName").trim();
        }

        if (searchParameters.containsKey("dateFrom") && !searchParameters.get("dateFrom").isEmpty()) {
            try {
                Long millisecondsBefore = Long.valueOf(searchParameters.get("dateFrom"));
                Timestamp stampBefore = new Timestamp(millisecondsBefore);
                dateFrom = new Date(stampBefore.getTime());
            } catch (NumberFormatException nfe) {
                System.out.println("Event Controller-> Date before is not added as a search parameter.");
            }
        }

        if (searchParameters.containsKey("dateTo") && !searchParameters.get("dateTo").isEmpty()) {
            try {
                Long millisecondsAfter = Long.valueOf(searchParameters.get("dateTo"));
                Timestamp stampAfter = new Timestamp(millisecondsAfter);
                dateTo = new Date(stampAfter.getTime());
            } catch (NumberFormatException nfe) {
                System.out.println("Event Controller-> Date After is not added as a search parameter.");
            }
        }

        List<EventStatus> eventStatuses = new ArrayList<>();

        for (EventStatus eventStatus : EventStatus.values()) {
            if (searchParameters.containsKey(eventStatus.toString().toLowerCase())) {
                eventStatuses.add(eventStatus);
            }
        }

        Collection<User> creators = null;
        if (searchParameters.containsKey("creatorName")) {
            creators = this.userRepository.findAllByNameContainingIgnoreCase(searchParameters.get("creatorName").trim());
        }

        Pageable pageRequest = this.createPageRequest(0);
        if (searchParameters.containsKey("pageNumber")) {
            pageRequest = this.createPageRequest(Integer.valueOf(searchParameters.get("pageNumber")));
        }

        EventSpecificationBuilder builder = new EventSpecificationBuilder();
        Specification<Event> specification = builder
                .with("name", SearchOperation.EQUALS, eventName)
                .or("status", SearchOperation.BY_STATUSES, eventStatuses)
                .withCreator("creator", SearchOperation.BY_CREATORS, creators)
                .inRangeOf("date", SearchOperation.DATE_FROM, dateFrom)
                .inRangeOf("date", SearchOperation.DATE_TO, dateTo)
                .build();

        Page<Event> resultEvents = this.eventRepository.findAll(specification, pageRequest);
        return resultEvents;
    }


    /**
     * This method must be used to visualize specific number
     * of events in Home page, if user is not logged in.
     *
     * @param size must be 1 or more. If size is less then 1
     *             will return Response with status 500 and
     *             {@see IllegalArgumentException}.
     * @return {@see Collection} with {@see Event}. The size
     * of this collection will be equal or less from size.
     * If the database has not existing events will return
     * empty collection.
     */
    @RequestMapping(path = "/limit/{size}", method = RequestMethod.GET)
    public Collection<Event> getEventsByStatusOpenOrderedByDateWithLimit(@Min(1) @NotNull @PathVariable(name = "size") int size) {
        Pageable top = new PageRequest(0, size);
        return this.eventRepository.findEventsByStatusOrderByDate(EventStatus.OPEN_FOR_ENROLLMENT, top);
    }

    /**
     * Changes the picture status for specific event
     *
     * @param data contains event id and status
     * @param user the currently logged in user
     * @return status ok on success
     */
    @RequestMapping(path = "picture-status", method = RequestMethod.PATCH)
    public ResponseEntity changePictureStatus(@RequestParam Map<String, String> data, @AuthenticationPrincipal User user) {
        if (!data.containsKey("event") || !data.containsKey("status")) {
            return ResponseEntity.badRequest().build();
        }
        Event event = this.eventRepository.findOne(data.get("event"));
        if (event == null) {
            return ResponseEntity.badRequest().build();
        }
        boolean status = Boolean.parseBoolean(data.get("status"));
        event.setHavingPicture(status);
        this.eventRepository.save(event);
        return ResponseEntity.ok().build();
    }


    /**
     * @return all values from the enum {@link EventStatus}
     */
    @RequestMapping(path = "/getAllStatuses", method = RequestMethod.GET)
    public Collection<String> getAllStatuses() {
        return Arrays.stream(EventStatus.values()).map(Enum::name)
                .collect(Collectors.toList());
    }

    /**
     * Used to serialize an event and add additional subscription type in order
     * to list both the event and the subscriber's status (GOING, INTERESTED).
     * <p>
     * Modifies myEventsList parameter.
     *
     * @param event                 the event
     * @param eventAndSubscriptions the list containing the events and the subscriptions to the them
     * @param currentUser           the currently logged in user
     */
    private void eventWithSubscriptionTypeToJson(Event event, List<Map<String, Object>> eventAndSubscriptions,
                                                 @AuthenticationPrincipal User currentUser) {
        ObjectMapper objectMapper = new ObjectMapper();
        User user = this.userRepository.findUserById(currentUser.getId());
        Map<String, Object> tempMap = objectMapper.convertValue(event, Map.class);
        EventSubscriber eventSubscriber = this.eventSubscriberRepository
                .findEventSubscriberBySubscriberAndEvent(user, event);
        if (eventSubscriber != null) {
            tempMap.putIfAbsent("subscriptionType", eventSubscriber.getSubscriptionType());
        }
        if (!eventAndSubscriptions.contains(tempMap)) {
            eventAndSubscriptions.add(tempMap);
        }
    }

    /**
     * Uses the group repository to check if all of the desired groups exist.
     *
     * @param eventGroups the groups which an event is associated with
     * @return true if even one of the input groups does not exist.
     */
    private boolean checkNonExistingGroup(Collection<Group> eventGroups) {
        Group groupFromDatabase = null;
        for (Group currentGroup : eventGroups) {
            groupFromDatabase = this.groupRepository.findByName(currentGroup.getName());
            if (groupFromDatabase == null) {
                return true;
            } else {
                currentGroup.setId(groupFromDatabase.getId());
            }
        }
        return false;
    }

    /**
     * Creates a new Page Request for the Events ordered ascending by
     * the event date with fixed fixed size of 10.
     *
     * @param page the number of the page
     * @return Pageable
     */
    private Pageable createPageRequest(int page) {
        return new PageRequest(page, 10, Direction.ASC, "date");
    }

}