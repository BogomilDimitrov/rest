package com.paysafe.evento.controllers;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.entites.UserGroupSubscription;
import com.paysafe.evento.repositories.UserGroupSubscriptionRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/user-groups")
public class UserGroupSubscriptionController {

    @Autowired
    private UserGroupSubscriptionRepository userGroupSubscriptionRepository;

    /**
     * Gets all user group subscription for the currently logged-in user.
     *
     * @param currentUser the currently logged-in user entity. It is passed by springboot annotation
     * @return Collection of UserGroupSubscription
     */
    @ApiOperation(value = "Get all user group subscriptions for the currently logged-in user.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved information."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(method = RequestMethod.GET)
    public List<UserGroupSubscription> getAllSubscribedGroupsForCurrentUser(@AuthenticationPrincipal User currentUser) {
        return (List<UserGroupSubscription>) this.userGroupSubscriptionRepository.findAllByUser(currentUser);
    }

    /**
     * Saves all User Group Subscription passed to the method for the currently logged-in user. Takes a list of
     * UserGroupSubscription as input parameter. The user entity is passed by the context with annotation.
     *
     * @param userGroupSubscriptions List of UserGroupSubscription as RequestBody (json)
     * @param user                   Currently logged-in user taken from the context
     * @return ResponseEntity - ok after successful save.
     */
    @ApiOperation(value = "Save all user group subscriptions passed to the method for the currently logged-in user.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully saved user group subscriptions."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(method = RequestMethod.PATCH)
    public ResponseEntity setAllUserSubscriptions(@RequestBody List<UserGroupSubscription> userGroupSubscriptions, @AuthenticationPrincipal User user) {
        userGroupSubscriptions.forEach(userGroup -> userGroup.setUser(user));
        this.userGroupSubscriptionRepository.save(userGroupSubscriptions);
        return ResponseEntity.ok().build();
    }

    /**
     * Get all events included in the current user's subscribed groups.
     *
     * @param currentUser the current user
     * @return collection of events
     */
    @ApiOperation(value = "Get all events included in the current user's subscribed groups.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved events."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/inMyGroups", method = RequestMethod.GET)
    public Collection<Event> getEventsInMyGroups(@AuthenticationPrincipal User currentUser) {
        Collection<UserGroupSubscription> subscriptions = this.userGroupSubscriptionRepository
                .findAllByUserAndSubscribed(currentUser, true);
        Collection<Event> result = new HashSet<>();
        for (UserGroupSubscription subscription : subscriptions) {
                Collection<Event> eventsFromOneGroup = subscription.getSubscribedGroup().getEvents();
                result.addAll(eventsFromOneGroup);
        }
        result = result.stream().filter((event) ->
                event.getStatus().equals(EventStatus.OPEN_FOR_ENROLLMENT) ||
                        event.getStatus().equals(EventStatus.CLOSED_FOR_ENROLLMENT))
                .collect(Collectors.toSet());
        return result;
    }
}
