package com.paysafe.evento.controllers;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.RollbackException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.paysafe.evento.entites.MailTemplates;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.entites.UserMailSubscription;
import com.paysafe.evento.entites.UserRole;
import com.paysafe.evento.repositories.MailTemplatesRepository;
import com.paysafe.evento.repositories.UserMailSubscriptionRepository;
import com.paysafe.evento.repositories.UserRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin("*")
@RequestMapping(path = "/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMailSubscriptionRepository userMailSubscriptionRepository;
    @Autowired
    private MailTemplatesRepository mailTemplatesRepository;

    @Value("${google.client.id}")
    private String clientId;

    @Value("${google.client.secret}")
    private String clientSecret;

    @ApiOperation(value = "Get all users.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved all users."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<User> getAll(@AuthenticationPrincipal User currentUser) {
        return this.userRepository.findAll();
    }

    /**
     * Method that obtain a authorize code from front-end and currently login User(if there is)
     * Using google API to obtain openIdToken , get the user profile from google , create new user with the retrieved payload
     * persist it in the DB.
     *
     * @param userDetails currently login user if there is
     * @param code        authorize code send from the fron-end after successfully login with google
     * @param redirectUri redirect URI according to google credentials
     * @return
     */
    @ApiOperation(value = "Authorize a user with user details and authorization code.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully authorized user."),
            @ApiResponse(code = 400, message = "Token response not found."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/authorize", method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> authorize(@AuthenticationPrincipal User userDetails, @RequestParam("code") String code, @RequestParam("redirect_uri") String redirectUri) {
        JsonFactory jsonFactory = new JacksonFactory();
        HttpTransport transport = new NetHttpTransport();
        GoogleTokenResponse tokenResponse = getGoogleTokenResponse(code, redirectUri, jsonFactory, transport);
        Map<String, String> response = new HashMap<>();
        if (tokenResponse != null) {
            userDetails = new User();
            try {
                GoogleIdToken.Payload payload = verifyIdTokenAndGetPayload(jsonFactory, transport, tokenResponse);
                userDetails = findUserByEmailAndCreateNewIfNotExists(userDetails, payload);
                setSecurityContextForCurrentLoginUser(userDetails);
                if(!userDetails.isTermsAccepted()){
                    response.put("uri", redirectUri);
                    response.put("termsAccepted", "false");
                    response.put("name", userDetails.getName());
                    response.put("pic", (String) payload.get("picture"));
                    response.put("id", userDetails.getId());
                }else{
                    response.put("uri", redirectUri);
                    response.put("termsAccepted", "true");
                    response.put("name", userDetails.getName());
                    response.put("pic", (String) payload.get("picture"));
                    response.put("id", userDetails.getId());
                }

            } catch (IOException | GeneralSecurityException e) {
                e.printStackTrace();
            }

            return ResponseEntity.ok(response);
        }
        return ResponseEntity.badRequest().build();
    }

    /**
     * Set the new user into the security context , creating a JSESSIONID between front-end, back-end
     *
     * @param userDetails currently Login user
     */
    private void setSecurityContextForCurrentLoginUser(@AuthenticationPrincipal User userDetails) {
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, null, Collections.emptyList());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @ApiOperation(value = "Checks if user is still logged in.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully checked user."),
            @ApiResponse(code = 400, message = "No such user."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(value = "/isStillLogin")
    private ResponseEntity<User> isUserStillLogin(@RequestParam Map<String,String> data, @AuthenticationPrincipal User user ){
        user = this.userRepository.findUserById(data.get("id"));
        if(user != null){
            setSecurityContextForCurrentLoginUser(user);
            return ResponseEntity.ok().build();
        }
        return  ResponseEntity.badRequest().build();

    }

    /**
     * Search into the DB , if the current user is already in the base, we give the details to the security Context,
     * otherwise we create a new user and set it to the security.
     *
     * @param userDetails currently Log in user
     * @param payload     all the data of user's google account
     */
    private User findUserByEmailAndCreateNewIfNotExists(@AuthenticationPrincipal User userDetails, GoogleIdToken.Payload payload) {
        User user = this.userRepository.findUserBySub(payload.getSubject());
        String name = (String) payload.get("name");
        String pictureUrl = (String) payload.get("picture");
        if (user == null) {
            userDetails.setName(name);
            userDetails.setPictureURL(pictureUrl);
            userDetails.setSub(payload.getSubject());
            userDetails.setEmail(payload.getEmail());
            userDetails.setRole(UserRole.USER);
            userDetails.setTermsAccepted(false);
            this.userRepository.save(userDetails);
            setAvailableMailSubscriptionsForUser(userDetails);
            userDetails.setUserMailSubscriptions(getAllMailSubscriptionsForUser(userDetails));
        } else {
            userDetails = user;
        }
        return userDetails;
    }

    /**
     * Verifying the openIdToken which produce GoogleIdToken, which contains the information about the user.
     *
     * @param jsonFactory   needs in order the method to work
     * @param transport     needs in order the method to work
     * @param tokenResponse contains idToken and accessToken
     * @return payload which contains the desired google account data
     * @throws GeneralSecurityException
     * @throws IOException
     */
    private GoogleIdToken.Payload verifyIdTokenAndGetPayload(JsonFactory jsonFactory, HttpTransport transport, GoogleTokenResponse tokenResponse) throws GeneralSecurityException, IOException {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
                .setAudience(Collections.singletonList(this.clientId))
                .build();
        GoogleIdToken googleIdToken =
                verifier.verify(tokenResponse.getIdToken());
        return googleIdToken.getPayload();
    }

    /**
     * Method that hit the google token end-point and obtains from it, the openIdToken.
     *
     * @param code        Authorize code that is obtained from front-end
     * @param redirectUri must be the same in google credentials
     * @param jsonFactory needs in order the method to work
     * @param transport   needs in order the method to work
     * @return TokenResponse which contains the openIdToken
     */
    private GoogleTokenResponse getGoogleTokenResponse(@RequestParam("code") String code, @RequestParam("redirect_uri") String redirectUri, JsonFactory jsonFactory, HttpTransport transport) {
        GoogleAuthorizationCodeTokenRequest authorizationCodeTokenRequest
                = new GoogleAuthorizationCodeTokenRequest(transport, jsonFactory, this.clientId, this.clientSecret, code, redirectUri);
        Collection<String> collection = new ArrayList<>();
        collection.add("profile");
        collection.add("email");
        authorizationCodeTokenRequest.setTokenServerUrl(new GenericUrl("https://www.googleapis.com/oauth2/v4/token"));
        authorizationCodeTokenRequest.setScopes(collection);


        GoogleTokenResponse tokenResponse = null;
        try {
            tokenResponse = authorizationCodeTokenRequest.execute();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return tokenResponse;
    }


    /**
     * Returns the user specified by the unique parameter Id.
     *
     * @param userId the Id of the user entry
     * @return User object or null
     */
    @ApiOperation(value = "Get user by ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved user."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(method = RequestMethod.GET, value = "users/{userId}")
    public User findById(@PathVariable(name = "userId") String userId) {
        return this.userRepository.findOne(userId);
    }

    /**
     * Returns the currently logged in user as object.
     * Used for getting the Id of the currently logged user in order to assign him to an event.
     *
     * @return User object
     */
    @ApiOperation(value = "Get current user.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved current user."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(method = RequestMethod.GET, value = "/currentUser")
    public User getCurrentUser(@AuthenticationPrincipal User user) {
        if (user.getUsername() != null) {
            user = this.userRepository.findUserByUsername(user.getUsername());
            return user;
        }
        if (user.getSub() != null) {
            user = this.userRepository.findUserBySub(user.getSub());
        }
        return user;
    }
    @RequestMapping(method = RequestMethod.POST, value = "/termsAndConditions")
    public ResponseEntity updateUserTermsAndConditions(@AuthenticationPrincipal User user , @RequestParam Map<String,String> data){
        if(data.get("terms").equalsIgnoreCase("true")){
            if(user.getSub() !=null){
                user = this.userRepository.findUserBySub(user.getSub());
                user.setTermsAccepted(true);
                this.userRepository.save(user);
                return  ResponseEntity.ok().build();
            }else if(user.getUsername() !=null){
                user = this.userRepository.findUserByUsername(user.getUsername());
                user.setTermsAccepted(true);
                this.userRepository.save(user);
                return  ResponseEntity.ok().build();
            }
        }
        return  ResponseEntity.badRequest().build();
    }

    /**
     * Changing current login user details , user email and user display name
     *
     * @param data obtain from front-end
     * @param currentUser currently login user
     * @return status OK.
     */
    @ApiOperation(value = "Change current login user details, user email and user display name.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully changed details."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(value = "/changeDetails", method = RequestMethod.PATCH)
    public ResponseEntity changeUserEmail(@RequestParam Map<String, String> data, @AuthenticationPrincipal User currentUser) {
        System.out.println(data.get("newEmail"));
        User user = this.userRepository.findUserById(currentUser.getId());
        if (!data.get("newEmail").equalsIgnoreCase("undefined")) {
            user.setEmail(data.get("newEmail"));
        }
        if (!data.get("displayName").equalsIgnoreCase("undefined")) {
            user.setName(data.get("displayName"));
        }
        this.userRepository.save(user);
        return ResponseEntity.ok().build();
    }

    /**
     * Sets all the available mail templates to true for the user upon creation.
     *
     * @param user - the current user to set the mail subscriptions to.
     */
    private void setAvailableMailSubscriptionsForUser(User user) {
        Collection<MailTemplates> templates = this.mailTemplatesRepository.findAll();
        for (MailTemplates template : templates) {
            UserMailSubscription userMailSubscription = new UserMailSubscription();
            userMailSubscription.setEnabled(true);
            userMailSubscription.setUser(user);
            userMailSubscription.setMailTemplates(template);
            this.userMailSubscriptionRepository.save(userMailSubscription);
        }
    }

    /**
     * Gets all User Mail Subscriptions for a given User
     *
     * @param user
     * @return - collection of UserMailSubscription
     */
    private Collection<UserMailSubscription> getAllMailSubscriptionsForUser(User user) {
        return this.userMailSubscriptionRepository.findAllByUser(user);
    }

    @RequestMapping(path = "/registerUser", method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> registerUser(@RequestBody User user) {
        Map<String, String> response = new HashMap<>();
        user.setRole(UserRole.USER);
        String passwordHash = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
        user.setPictureURL("https://lh3.googleusercontent.com/H9yAIsZYqbIOh_E1ON90chVhO6SYSD6ucV-XirZXkMFDqLRjGoztobaxx1XS9CB4lfg=w300-rw");
        user.setPassword(passwordHash);
        this.userRepository.save(user);
        setAvailableMailSubscriptionsForUser(user);
        user.setUserMailSubscriptions(getAllMailSubscriptionsForUser(user));
        user = this.userRepository.findUserByUsername(user.getUsername());
        setSecurityContextForCurrentLoginUser(user);
        response.put("id", user.getId());
        response.put("username", user.getUsername());
        response.put("name", user.getName());
        response.put("pic", user.getPictureURL());

        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "/signIn", method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> signIn(@RequestBody User user) {
        Map<String, String> response = new HashMap<>();
        User checkedUser = this.userRepository.findUserByUsername(user.getUsername());
        boolean isMatchedPassword = BCrypt.checkpw(user.getPassword(), checkedUser.getPassword());
        if (isMatchedPassword) {
            setSecurityContextForCurrentLoginUser(checkedUser);
            response.put("username", checkedUser.getUsername());
            response.put("id", checkedUser.getId());
            response.put("name", checkedUser.getName());
            response.put("pic", checkedUser.getPictureURL());
            return ResponseEntity.ok(response);
            }

        response.put("authorize", "failed");
        return ResponseEntity.badRequest().body(response);

    }

    @RequestMapping(path = "/getAll", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAllUsernames() {
        List<User> users = (List) this.userRepository.findAll();
        for (int i = 0; i < users.size(); i++) {
            users.get(i).setPassword(null);
            users.get(i).setRole(null);
            users.get(i).setRole(null);
        }
        return ResponseEntity.ok(users);
    }

    @RequestMapping(value = "/changeRole", method = RequestMethod.PATCH)
    public ResponseEntity changeUserRole(@RequestParam Map<String, String> data, @AuthenticationPrincipal User user) {
        User userToEdit = this.userRepository.findOne(data.get("id"));
        userToEdit.setRole(UserRole.valueOf(data.get("newRole")));
        try {
            this.userRepository.save(userToEdit);
            return ResponseEntity.ok(userToEdit);
        } catch (RollbackException e) {
            return ResponseEntity.badRequest().build();
        }
    }
}