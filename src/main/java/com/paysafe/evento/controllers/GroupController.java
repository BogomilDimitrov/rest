package com.paysafe.evento.controllers;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.Group;
import com.paysafe.evento.entites.UserGroupSubscription;
import com.paysafe.evento.repositories.EventRepository;
import com.paysafe.evento.repositories.GroupRepository;
import com.paysafe.evento.repositories.UserGroupSubscriptionRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.RollbackException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/groups")
@CrossOrigin("*")
public class GroupController {

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private UserGroupSubscriptionRepository userGroupSubscriptionRepository;

    /**
     * @return Iterable of existing groups.
     */
    @ApiOperation(value = "Get all groups.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved all groups."),
            @ApiResponse(code = 400, message = "User does not exist."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Group> getAllGroups() {
        fillGroupsInDatabase();
        return this.groupRepository.findAll();
    }


    /**
     * Creates dummy group for testing purposes.
     * This method will be deleted after group addding implementation.
     * <p>It will be proccessed only if the group table is empty.</p>
     */
    private void fillGroupsInDatabase() {
        List<Group> groups = Arrays.asList(
                new Group("Developers"),
                new Group("Party"),
                new Group("Conference"),
                new Group("Marketing")
        );
        long existingEvents = this.groupRepository.count();
        if (existingEvents == 0) {
            this.groupRepository.save(groups);
        }
    }


    @ApiOperation(value = "Add group.")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addGroup(@RequestParam Map<String, String> data) {
        Group group = new Group(data.get("groupName"));
        try {
            this.groupRepository.save(group);
            return ResponseEntity.ok(group);
        } catch (RollbackException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @ApiOperation(value = "Remove group.")
    @RequestMapping(path = "/remove/{groupName}", method = RequestMethod.DELETE)
    public ResponseEntity deleteGroup(@PathVariable(value = "groupName") String groupName) {
        Group group = this.groupRepository.findByName(groupName);
        for (Event event : this.eventRepository.findAll()) {
            if (event.getAssignedGroups().contains(group)) {
                event.getAssignedGroups().remove(group);
                this.eventRepository.save(event);
            }
        }
        for (UserGroupSubscription g : this.userGroupSubscriptionRepository.findAll()) {
            if (g.getSubscribedGroup().equals(group)) {
                this.userGroupSubscriptionRepository.delete(g);
            }
        }
        try {
            this.groupRepository.delete(group);
            return ResponseEntity.ok(group);
        } catch (RollbackException e) {
            return ResponseEntity.badRequest().build();
        }
    }
}
