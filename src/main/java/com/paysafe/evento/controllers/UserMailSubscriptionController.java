package com.paysafe.evento.controllers;

import com.paysafe.evento.entites.User;
import com.paysafe.evento.entites.UserMailSubscription;
import com.paysafe.evento.repositories.UserMailSubscriptionRepository;
import com.paysafe.evento.repositories.UserRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
@CrossOrigin("*")
public class UserMailSubscriptionController {

    @Autowired
    private UserMailSubscriptionRepository userMailSubscriptionRepository;
    @Autowired
    private UserRepository userRepository;

    @ApiOperation(value = "Get user mail subscriptions.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved user mail subscriptions."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/userNotifications", method = RequestMethod.GET)
    public List<UserMailSubscription> getUserMailSubscriptions(@AuthenticationPrincipal User currentUser) {
        User user = this.userRepository.findUserById(currentUser.getId()) ;
        List<UserMailSubscription> userMailSubscriptions =
                this.userMailSubscriptionRepository.findAllByUser(user);
        return userMailSubscriptions;
    }

    @ApiOperation(value = "Edit notification settings.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully edited notification settings."),
            @ApiResponse(code = 400, message = "Tempate is missing."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/editNotifications", method = RequestMethod.PATCH)
    public ResponseEntity editNotificationSettings(@RequestBody List<UserMailSubscription> userMailSubscription) {
        this.userMailSubscriptionRepository.save(userMailSubscription);
        return ResponseEntity.ok().build();
    }
}
