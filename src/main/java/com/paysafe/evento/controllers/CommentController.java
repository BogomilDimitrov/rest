package com.paysafe.evento.controllers;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.paysafe.evento.entites.Comment;
import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.repositories.CommentRepository;
import com.paysafe.evento.repositories.EventRepository;
import com.paysafe.evento.repositories.UserRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Comments")
@RestController
@RequestMapping(path = "/comments")
@CrossOrigin("*")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private UserRepository userRepository;

    /**
     * Retrieves all existing comments per event ordered by date of submission.
     * If the event does not exist returns an empty collection.
     *
     * @param eventId the event id
     * @return all existing comments ordered by date  descending.
     */
    @ApiOperation(value = "Get all existing comments for an event.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully retrieved comments."),
            @ApiResponse(code = 400, message = "Invalid event data provided."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")})
    @RequestMapping(value = "{eventId}", method = RequestMethod.GET)
    public Collection<Comment> getCommentsByEventSortedByDateOfCreation(
            @PathVariable(name = "eventId") String eventId) {
        Event event = this.eventRepository.findById(eventId);
        if (event != null) {
            return commentRepository.findAllByEventOrderByDateOfCreationDesc(event);
        }
        return new HashSet<Comment>();
    }

    /**
     * Checks for an existing event by id.
     * If the event does not exist or the parameter commentContent is an empty string
     * returns {@See ResponseEntity} with bad request and an error message.
     * <p>
     * Otherwise saves the input data as {@See Comment} returns status OK and the new comment.
     *
     * @param commentContent the content of the comment
     * @param timeOfPosting  the exact time of submission
     * @param eventId        the id of the event
     * @return Response Entity
     */
    @ApiOperation(value = "Post a comment under an event.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successfully posted comment."),
            @ApiResponse(code = 400, message = "Event does not exist."),
            @ApiResponse(code = 400, message = "Cannot add comment with no content."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")})
    @RequestMapping(method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {
            MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @ResponseBody
    public ResponseEntity postAComment(@NotNull @RequestParam("commentContent") String commentContent,
                                       @NotNull @RequestParam("timeOfPosting") String timeOfPosting,
                                       @NotNull @RequestParam("eventId") String eventId, @AuthenticationPrincipal User currentUser) {
        Event existingEvent = this.eventRepository.findOne(eventId);

        if (existingEvent == null) {
            return ResponseEntity.badRequest().body("This event doesn't exist");
        }
        if (commentContent.trim().equals("")) {
            return ResponseEntity.badRequest().body("Cannot add comment with no content");
        }

        Comment comment = new Comment();
        comment.setCreator(this.userRepository.findUserById(currentUser.getId()));
        comment.setContent(commentContent);
        comment.setDateOfCreation(Timestamp.valueOf(timeOfPosting));
        comment.setEvent(existingEvent);
        this.commentRepository.save(comment);

        return ResponseEntity.ok(comment);
    }
}
