package com.paysafe.evento.controllers;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.entites.EventSubscriber;
import com.paysafe.evento.entites.SubscriptionType;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.repositories.EventRepository;
import com.paysafe.evento.repositories.EventSubscriberRepository;
import com.paysafe.evento.repositories.UserRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/feedback")
@CrossOrigin("*")
public class FeedbackController {

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private EventSubscriberRepository eventSubscriberRepository;
    @Autowired
    private UserRepository userRepository;

    /**
     * Retrieves eventSubscribe and Event by event ID
     * The result contains two entries.
     * <p>
     * The first one has a String "feedbacks"
     * as key and all the {@link EventSubscriber}s that have not yet given feedback.
     * {@link #calculateAverageRating(String)} is called to correspond to
     * the second entries - "rating".
     *
     * @param id the event id
     * @return Response entity with a map with two key-value pairs.
     */
    @ApiOperation(value = "Gets eventSubscriber and average rating for the event by ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved eventSubscribe and Event by ID."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getAllFeedbackAndAverageRating(@PathVariable(name = "id") String id,
                                                                              @AuthenticationPrincipal User currentUser) {
        Event event = this.eventRepository.findById(id);
        Collection<EventSubscriber> eventSubscribers =
                this.eventSubscriberRepository.findByEventAndFeedbackCommentIsNotNullAndFeedbackRatingIsNotNull(event);
        Map<String, Object> feedbackAndAverageRating = new HashMap<>();
        feedbackAndAverageRating.put("feedbacks", eventSubscribers);
        feedbackAndAverageRating.put("rating", this.calculateAverageRating(id));
        return ResponseEntity.ok(feedbackAndAverageRating);
    }

    /**
     * Adds feedback for the event with the specified ID
     *
     * @param encodedData map containing eventId, rating and comment that the current user is given.
     * @return Response(200) if the eventSubscriber is successfully updated or response(400) if
     * encoded data don't have the required keys or rating is not a number between 1-5.
     */
    @ApiOperation(value = "Adds feedback from the current event subscriber.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added feedback."),
            @ApiResponse(code = 400, message = "Rating is invalid."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<?> addFeedback(@RequestParam Map<String, String> encodedData,
                                         @AuthenticationPrincipal User currentUser) {
        if (!(encodedData.containsKey("eventId") && encodedData.containsKey("comment") &&
                encodedData.containsKey("rating") && encodedData.get("rating").matches("^[1-5]"))) {
            return ResponseEntity.badRequest().build();
        }
        Event event = this.eventRepository.findById(encodedData.get("eventId"));
        if (event == null) {
            return ResponseEntity.badRequest().build();
        }
        if (event.getStatus() != EventStatus.CLOSED) {
            return ResponseEntity.badRequest().body("The event should be closed.");
        }
        User user = this.userRepository.findOne(currentUser.getId());
        EventSubscriber eventSubscriber = this.eventSubscriberRepository
                .findEventSubscriberBySubscriberAndEvent(user, event);
        if (eventSubscriber.getFeedbackComment() != null || eventSubscriber.getFeedbackRating() != 0 ||
                eventSubscriber == null) {
            return ResponseEntity.badRequest().build();
        }
        eventSubscriber.setFeedbackComment(encodedData.get("comment"));
        eventSubscriber.setFeedbackRating(Integer.parseInt(encodedData.get("rating")));
        this.eventSubscriberRepository.save(eventSubscriber);
        return ResponseEntity.ok().build();
    }

    /**
     * Gets information for specific Event by ID:
     * <ol>
     * <li>whether the user had given feedback;</li>
     * <li>average rating for the event.</li>
     * </ol>
     *
     * @param eventId ID of the event which will be searched against
     * @return Key-value pair containing the following
     * <ol>
     * <li>'hasFeedback' : boolean</li>
     * <li>'rating' : number</li>
     * </ol>
     */
    @ApiOperation(value = "Retrieves information for event.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved information for event."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(value = "/information/{eventId}", method = RequestMethod.GET)
    public Map<String, String> retrieveInformationForEvent(@PathVariable(name = "eventId") String eventId,
                                                           @AuthenticationPrincipal User currentUser) {
        User user = this.userRepository.findOne(currentUser.getId());
        Event event = this.eventRepository.findById(eventId);
        Map<String, String> map = new HashMap<>();
        if (null == event) {
            return map;
        }
        Collection<EventSubscriber> ratingSubscribers = this.eventSubscriberRepository.findEventSubscribersByEvent(event);
        EventSubscriber eventSubscriber = this.eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, event);

        if (eventSubscriber == null || (eventSubscriber.getFeedbackComment() == null && eventSubscriber.getFeedbackRating() == 0)) {
            map.put("hasFeedback", "false");
        } else {
            map.put("hasFeedback", "true");
        }
        int sum = 0;
        int subscribersCounter = 0;
        for (EventSubscriber es : ratingSubscribers) {
            if (es.getFeedbackRating() != 0) {
                sum += es.getFeedbackRating();
                subscribersCounter++;
            }
        }
        if (sum != 0) {
            sum = (int) Math.round((double) sum / subscribersCounter);
        }
        map.put("rating", Integer.toString(sum));
        return map;
    }

    /**
     * Calculates the average rating for the event, requested by ID.
     *
     * @param eventId ID of the event which will be searched against
     * @return The average rating for the event based on subscribed users feedback.
     */
    @ApiOperation(value = "Get average rating.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved average rating."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/eventRating/{eventId}", method = RequestMethod.GET)
    public ResponseEntity<Double> getAverageRating(@PathVariable("eventId") String eventId) {
        Double sum = calculateAverageRating(eventId);
        return ResponseEntity.ok(sum);
    }

    private Double calculateAverageRating(String eventId) {
        Collection<EventSubscriber> subscribers = this.eventSubscriberRepository.findEventSubscribersByEventId(eventId);
        Double sum = 0.0;
        int counter = 0;
        if (subscribers.size() > 0) {
            for (EventSubscriber subscriber : subscribers) {
                if (subscriber.getFeedbackRating() > 0) {
                    counter++;
                    sum += subscriber.getFeedbackRating();
                }
            }
            return sum / counter;
        }
        return sum;
    }

    /**
     * Checks whether the user should give feedback or not.
     *
     * @return JSON with key 'feedbackAvailable' and value:
     * true if the event is closed, the user hadn't given feedback already and the user is GOING or INTERESTED;
     * false if either statement is false
     */
    @ApiOperation(value = "Checks whether the user should give feedback or not.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved confirmation."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/{eventId}/feedbackAvailable", method = RequestMethod.GET)
    public ResponseEntity<?> shouldGiveFeedback(@PathVariable("eventId") String eventId,
                                                @AuthenticationPrincipal User currentUser) {
        Map<String, Boolean> isFeedbackAvailable = new HashMap<>();
        Event event = this.eventRepository.findById(eventId);
        if (event == null) {
            return ResponseEntity.notFound().build();
        }
        User user = this.userRepository.findOne(currentUser.getId());
        EventSubscriber subscription = this.eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, event);
        if (subscription == null) {
            isFeedbackAvailable.put("feedbackAvailable", false);
            return ResponseEntity.ok(isFeedbackAvailable);
        }
        if (subscription.getSubscriptionType() != SubscriptionType.NOT_GOING && subscription.getFeedbackRating() != 0 ||
                subscription.getFeedbackComment() != null || subscription.getEvent().getStatus() != EventStatus.CLOSED) {
            isFeedbackAvailable.put("feedbackAvailable", false);
        } else {
            isFeedbackAvailable.put("feedbackAvailable", true);
        }
        return ResponseEntity.ok(isFeedbackAvailable);
    }
}