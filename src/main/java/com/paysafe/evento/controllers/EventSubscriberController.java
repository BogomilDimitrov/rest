package com.paysafe.evento.controllers;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.entites.EventSubscriber;
import com.paysafe.evento.entites.SubscriptionType;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.repositories.EventRepository;
import com.paysafe.evento.repositories.EventSubscriberRepository;
import com.paysafe.evento.repositories.UserRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/subscribe")
@CrossOrigin("*")
public class EventSubscriberController {

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private EventSubscriberRepository eventSubscriberRepository;
    @Autowired
    private UserController userController;
    @Autowired
    private UserRepository userRepository;

    @ApiOperation(value = "Gets all event subscribers.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved all event subscribers."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public Iterable<EventSubscriber> getAll() {
        return this.eventSubscriberRepository.findAll();
    }

    /**
     * Uses EventSubscription repository to retrieve all subscriptions for an event
     * regardless of the subscription type.
     *
     * @param eventId the event Id
     * @return all created subscriptions for the event
     */
    @ApiOperation(value = "Retrieve all subscriptions for an event regardless of the subscription type.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved all subscriptions."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/{eventId}", method = RequestMethod.GET)
    public Collection<EventSubscriber> getEventSubscriptions(@PathVariable("eventId") String eventId) {
        return this.eventSubscriberRepository.findEventSubscribersByEventId(eventId);
    }

    /**
     * Service handling user subscription for specific event and adjusting the subscription type
     * based on parameters taken in x-www-form-urlencoded format - subscriptionType and eventId.
     *
     * @param encodedInfo Mapped parameters taken from Patch request in form-urlencoded format -
     *                    subscriptionType and eventId.
     * @return ResponseEntity
     */
    @ApiOperation(value = "Subscribe a user for an event with a specific subscription type.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully subscribed to event."),
            @ApiResponse(code = 400, message = "Event/User is null or event status is not OPEN."),
            @ApiResponse(code = 400, message = "Illegal subscription type."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<EventSubscriber> subscribe(@RequestParam Map<String, String> encodedInfo,
                                                     @AuthenticationPrincipal User currentUser) {
        if (!(encodedInfo.containsKey("eventId") && encodedInfo.containsKey("subscriptionType"))) {
            return ResponseEntity.badRequest().build();
        }
        User user = this.userRepository.findUserById(currentUser.getId());
        Event event = this.eventRepository.findById(encodedInfo.get("eventId"));
        if (user == null || event == null || event.getStatus() != EventStatus.OPEN_FOR_ENROLLMENT) {
            return ResponseEntity.badRequest().build();
        }
        SubscriptionType subscriptionType;
        try {
            subscriptionType = SubscriptionType.valueOf(encodedInfo.get("subscriptionType").toUpperCase());
        } catch (IllegalArgumentException iae) {
            return ResponseEntity.badRequest().build();
        }
        EventSubscriber eventSubscriber = this.eventSubscriberRepository
                .findEventSubscriberBySubscriberAndEvent(user, event);
        if (eventSubscriber == null) {
            eventSubscriber = createNewEventSubscriber(user, event);
        }
        eventSubscriber.setSubscriptionType(subscriptionType);
        this.eventSubscriberRepository.save(eventSubscriber);
        return ResponseEntity.ok().build();
    }

    /**
     * Gets all subscribed events and the subscription type for a specific user. Returned in map format
     * - <String - eventId, String - subscriptionType>
     *
     * @param userId the unique Id of the User
     * @return Map<String,String> of EventIds and Subscription Types
     */

    @ApiOperation(value = "Get all subscribed events and subscription type for a specific user.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved subscribed events and subscription type for a specific user."),
            @ApiResponse(code = 400, message = "User does not exist."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/all/{userId}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, String>> getAllEventIdsForUser(@PathVariable(value = "userId") String userId) {
        User user = this.userController.findById(userId);
        if (user == null) {
            return ResponseEntity.badRequest().build();
        }
        Collection<EventSubscriber> allEventSubscribers = user.getSubscribedEvents();
        Map<String, String> allIds = new HashMap<>();
        for (EventSubscriber es : allEventSubscribers) {
            allIds.put(es.getEvent().getId(), es.getSubscriptionType().toString());
        }
        return ResponseEntity.ok(allIds);
    }

    /**
     * Takes current user from the Spring Security by @AuthenticationPrincipal
     * and checks his subscription type for the event.
     *
     * @param eventId the id of the event
     * @return Subscription type for the event.
     */
    @ApiOperation(value = "Get event subscription.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved event subscription."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/userSubscribeType/{eventId}", method = RequestMethod.GET)
    public ResponseEntity<SubscriptionType> getEventSubscription(@AuthenticationPrincipal User currentUser, @PathVariable("eventId") String eventId) {
        User user = this.userRepository.findUserById(currentUser.getId());
        Event event = this.eventRepository.findById(eventId);
        EventSubscriber subscriber = this.eventSubscriberRepository
                .findEventSubscriberBySubscriberAndEvent(user, event);
        if (subscriber != null) {
            return ResponseEntity.ok(subscriber.getSubscriptionType());
        } else {
            return ResponseEntity.ok(SubscriptionType.NOT_GOING);
        }
    }


    /**
     * @return all values from the enum {@link SubscriptionType}
     */
    @ApiOperation(value = "Retrieve all subscription types for an event.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved all subscription types."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(path = "/getAllSubscriptionTypes", method = RequestMethod.GET)
    public Collection<String> getAllSubscriptionTypes() {
        return Arrays.stream(SubscriptionType.values()).map(Enum::name)
                .collect(Collectors.toList());
    }

    /**
     * Creates new event subscriber for user - event combination.
     *
     * @param user  User entity
     * @param event Event entity
     * @return EventSubscriber entity with set user and event fields
     */
    private EventSubscriber createNewEventSubscriber(User user, Event event) {
        EventSubscriber eventSubscriber = new EventSubscriber();
        eventSubscriber.setEvent(event);
        eventSubscriber.setSubscriber(user);
        return eventSubscriber;
    }
}