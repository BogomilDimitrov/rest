package com.paysafe.evento.controllers;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.MailTemplates;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.entites.UserMailSubscription;
import com.paysafe.evento.repositories.EventRepository;
import com.paysafe.evento.repositories.MailTemplatesRepository;
import com.paysafe.evento.repositories.UserMailSubscriptionRepository;
import com.paysafe.evento.repositories.UserRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * IOController class is using for upload of the different HTML
 * templates which will be used from the email service.
 */
@RestController
@CrossOrigin(origins = "*")
public class IOController {
    private static final String SAVE_LOCATION_PATH = IOController.class
            .getClassLoader().getResource("templates").getPath();
    @Autowired
    private MailTemplatesRepository mailTemplatesRepository;
    @Autowired
    private UserMailSubscriptionRepository userMailSubscriptionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EventRepository eventRepository;
    private byte[] fileBytes;


    @ApiOperation(value = "Show template form.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully showed template form."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @GetMapping
    public String showTemplateForm() {
        return "uploadForm";
    }

    @ControllerAdvice
    public class MyErrorController extends ResponseEntityExceptionHandler {

        Logger logger = org.slf4j.LoggerFactory.getLogger(getClass());

        @ExceptionHandler(MultipartException.class)
        @ResponseBody
        String handleFileException(HttpServletRequest request, Throwable ex) {
            return "Picture upload error";
        }
    }

    /**
     * This parameters comes from user input that is using uploadForm.html file.
     *
     * @return ResponseEntity
     * @headers subject - The subject of the email that will be used for all the email with the same type.
     * @headers type    - The type of the email template is also the the name of the file after the upload.
     */
    @ApiOperation(value = "Handle file upload.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully handled file upload."),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource.")
    })
    @RequestMapping(value = "templates/upload", method = RequestMethod.POST)
    public ResponseEntity handleFileUpload(MultipartHttpServletRequest request) {

        String type = request.getHeader("Type");
        String subject = request.getHeader("Subject");
        Iterator<String> itr = request.getFileNames();
        MultipartFile file = request.getFile(itr.next());
        MailTemplates checkType = this.mailTemplatesRepository.findByType(type);

        if (file.getContentType().equalsIgnoreCase("text/html")) {
            try {
                Iterable<User> users = this.userRepository.findAll();
                String filePath = SAVE_LOCATION_PATH + File.separator + type + ".html";
                MailTemplates template = this.mailTemplatesRepository.findByType(type);
                if (template != null) {
                    fillTemplate(type, subject, filePath, template);
                    this.mailTemplatesRepository.save(template);
                    for (User user : users) {
                        UserMailSubscription userMailSubscription = this.userMailSubscriptionRepository.
                                findFirstByMailTemplatesAndUser(template, user);
                        fillUserMailSubscription(template, user, userMailSubscription);
                        this.userMailSubscriptionRepository.save(userMailSubscription);
                    }
                } else {
                    template = new MailTemplates();
                    fillTemplate(type, subject, filePath, template);
                    this.mailTemplatesRepository.save(template);

                    for (User user : users) {
                        UserMailSubscription userMailSubscription = new UserMailSubscription();
                        fillUserMailSubscription(template, user, userMailSubscription);
                        this.userMailSubscriptionRepository.save(userMailSubscription);
                    }
                }
                saveFileOnLocal(type, file);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok("Template added.");
    }

    /**
     * saveFileOnLocal() is used by handleFileUpload() to save the file in the resource directory.
     *
     * @param type - Type of the template which is also the name that the file will be saved with.
     * @param file - The exact file to save.
     */
    private void saveFileOnLocal(String type, MultipartFile file) {
        String templateLocation = this.mailTemplatesRepository.findByType(type).getFilePath();
        String htmlToBeProcessed = null;
        try {
            htmlToBeProcessed = new String(file.getBytes(), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        File tmpHtml = new File(templateLocation);
        try (PrintWriter out = new PrintWriter(tmpHtml)) {
            out.write(htmlToBeProcessed);
            out.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Uploads file to the web app assets images folder.
     *
     * @param request MultipartHttpServletRequest contains the image data
     * @return ResponseEntity
     * @throws IOException
     */
    @RequestMapping(value = "/upload-picture", method = RequestMethod.POST)
    public ResponseEntity<String> UploadFile(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> itr = request.getFileNames();
        MultipartFile file = request.getFile(itr.next());
        String eventId = file.getName();
        Event event = eventRepository.findById(eventId);
        File dir = new File("/var/lib/jenkins/workspace/web-build/src/assets/images");
        String picFormat = ".jpg";
        if (!event.isHavingPicture()) {
            event.setHavingPicture(true);
            eventRepository.save(event);
        }
        String picName = eventId + picFormat;
        if (dir.isDirectory()) {
            File serverFile = new File(dir, picName);
            FileOutputStream fos = new FileOutputStream(serverFile);
            fos.write(file.getBytes());
            fos.close();
            return ResponseEntity.ok().body(picName);
        }
        return ResponseEntity.badRequest().
                body("Could not upload file.");
    }

    @RequestMapping(path = "templates/getAll", method = RequestMethod.GET)
    public ResponseEntity getAllTemplates() {
        Collection<MailTemplates> mailTemplates = (List<MailTemplates>) this.mailTemplatesRepository.findAll();
        return ResponseEntity.ok(mailTemplates);
    }

    public void fillUserMailSubscription(MailTemplates template, User user, UserMailSubscription userMailSubscription) {
        userMailSubscription.setEnabled(true);
        userMailSubscription.setMailTemplates(template);
        userMailSubscription.setUser(user);
    }

    public void fillTemplate(String type, String subject, String filePath, MailTemplates template) {
        template.setFilePath(filePath);
        template.setType(type);
        template.setSubject(subject);
    }

    @RequestMapping(value = "/template/download/{name}", method = RequestMethod.GET)
    public HttpEntity<byte[]> exportReportInExcelFile(@PathVariable(name = "name") String name) {
        String templatePath;
        File file;
        FileInputStream inputStream;
        String fileName;

        if (name.equalsIgnoreCase("default_templates")) {
            templatePath = SAVE_LOCATION_PATH + File.separator +
                    "default_templates.7z";
            file = new File(templatePath);
            fileName = "default_templates.7z";
        } else {
            templatePath = SAVE_LOCATION_PATH + File.separator
                    + name + ".html";
            file = new File(templatePath);
            fileName = "Current template for " + name + ".html";
        }
        try {
            System.out.println(templatePath);
            inputStream = new FileInputStream(file);
            this.fileBytes = new byte[(int) file.length()];
            inputStream.read(this.fileBytes);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=" + fileName);
        header.setContentLength(fileBytes.length);
        return new HttpEntity<>(fileBytes, header);

    }
}