package com.paysafe.evento.mail;

import com.paysafe.evento.entites.*;
import com.paysafe.evento.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * EmailServiceImpl class is using for sending e-mails.
 */
@Service
public class EmailServiceImpl {

    private static final String EMAIL_SENDER_ADDRESS = "evento@paysafe";
    private MimeMessage mimeMessage;

    @Autowired
    private JavaMailSender sender;
    @Autowired
    private MailContentBuilder mailContentBuilder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MailTemplatesRepository mailTemplatesRepository;
    @Autowired
    private EventSubscriberRepository eventSubscriberRepository;
    @Autowired
    private UserMailSubscriptionRepository userMailSubscriptionRepository;
    @Autowired
    private UserGroupSubscriptionRepository userGroupSubscriptionRepository;

    /**
     * Sends email to all users.
     *
     * @param type  The type will define which HTML template will be used to
     *              pass the data to it (e.g. "NEW", "CHANGED").
     * @param event The event name, description and link will be passed in the HTML.
     */
    public void sendMailToAllUsers(String type, Event event) {
        this.sendEmailByType(type, event, (Collection<User>) this.userRepository.findAll());
    }

    /**
     * Sends email to all users that have subscribed to an event as GOING or INTERESTED.
     *
     * @param type  The type will define which HTML template will be used to
     *              pass the data to it (e.g. "NEW", "CHANGED").
     * @param event The event name, description and link will be passed in the HTML.
     */
    public void sendMailToSubscribedUsers(String type, Event event) {
        List<SubscriptionType> subscriptionTypes = new ArrayList<>();
        subscriptionTypes.add(SubscriptionType.GOING);
        subscriptionTypes.add(SubscriptionType.INTERESTED);
        Collection<EventSubscriber> eventSubscribers =
                this.eventSubscriberRepository.findEventSubscribersByEventAndSubscriptionTypeIn
                        (event,subscriptionTypes);
        Collection<User> subscribedUsers = eventSubscribers.stream()
                .map(EventSubscriber::getSubscriber)
                .collect(Collectors.toList());
        this.sendEmailByType(type, event, subscribedUsers);
    }

    /**
     * Base method to be used to send email to a specific set of users.
     * <p>
     * It also invokes Build() form MailContentBuilder class
     * to pass to customize the HTML template.
     *
     * @param type        The type will define which HTML template will be used to
     *                    pass the data to it (e.g. "NEW", "CHANGED").
     * @param event       The event name, description and link will be passed in the HTML.
     * @param targetUsers The users to whom the email will be send.
     */
    private void sendEmailByType(String type, Event event, Collection<User> targetUsers) {
        this.mimeMessage = this.sender.createMimeMessage();
        MailTemplates mailTemplate = this.mailTemplatesRepository.findByType(type);

        try {
            for (User user : targetUsers) {
                boolean sendEmailForSpecificGroup = false;
                UserMailSubscription userMailSubscription =
                        this.userMailSubscriptionRepository.findFirstByMailTemplatesAndUser(mailTemplate, user);
                Collection<UserGroupSubscription> userGroupSubscriptions =
                        this.userGroupSubscriptionRepository.findAllByUserAndNotifiedAndSubscribed(user,true,true);
                for (UserGroupSubscription userGroup :
                        userGroupSubscriptions) {
                    for (Group eventGroup :
                            event.getAssignedGroups()) {
                        if (userGroup.getSubscribedGroup().getName().equals(eventGroup.getName())){
                            sendEmailForSpecificGroup = true;
                        }
                    }
                }
                boolean isEnabled = userMailSubscription.isEnabled();
                if (user.isEnabledNotifications() && isEnabled) {
                    sendMailToOneUser(type, event, mailTemplate, user);
                }else if(user.isEnabledNotifications() && sendEmailForSpecificGroup){
                    sendMailToOneUser(type, event, mailTemplate, user);
                }
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Base method to send email to a single user.
     *
     * @param type         The type will define which HTML template will be used to
     *                     pass the data to it (e.g. "NEW", "CHANGED").
     * @param event        The event name, description and link will be passed in the HTML.
     * @param mailTemplate The template which will be sent
     * @param user         The receiver of the email
     * @throws MessagingException in case of errors
     */
    private void sendMailToOneUser(String type, Event event,
                                   MailTemplates mailTemplate, User user) throws MessagingException {
        MimeMessageHelper helper = new MimeMessageHelper(this.mimeMessage);
        String content = this.mailContentBuilder.build(event.getName(), event.getDescription(),
                event.getLink(), type, user.getName(), event.getId());
        helper.setSubject(mailTemplate.getSubject());
        helper.setText(content, true);
        helper.setFrom(EMAIL_SENDER_ADDRESS);
        helper.setTo(user.getEmail());
        this.sender.send(this.mimeMessage);
    }

}
