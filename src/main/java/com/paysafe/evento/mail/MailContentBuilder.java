package com.paysafe.evento.mail;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

/**
 *	MailContentBuilder class is using for customizing an existing HTML file with custom data.
 */
@Service
public class MailContentBuilder {
	
	private final static String HTML_TEMPLATE_SUFFIX = ".html";

	@Autowired
	private Configuration configuration;

	/**
	 * @param eventName - The name of the Event that will be passed in the HTML.
	 * @param description - The description of the Event that will be passed in the HTML.
	 * @param link - Link for the Event  that will be passed in the HTML.
	 * @param type - The type will define which HTML template will be used to pass the data to it.
	 * @return - Modified HTML with all the data passed.
	 */
	public String build(String eventName, String description,
                        String link, String type, String userName,String eventId) {
		StringBuffer content = new StringBuffer();
        Map<String, Object> context = new HashMap<>();
        context.put("name", eventName);
        context.put("description", description);
        context.put("link", link);
        context.put("username", userName);
        context.put("eventId", eventId);
		try {
			content.append(FreeMarkerTemplateUtils
			.processTemplateIntoString(this.configuration.getTemplate(type + HTML_TEMPLATE_SUFFIX),context));
		} catch (TemplateNotFoundException | TemplateException |
				MalformedTemplateNameException | ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content.toString();
	}
}
