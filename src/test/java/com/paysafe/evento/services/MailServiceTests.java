package com.paysafe.evento.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.paysafe.evento.entites.*;
import com.paysafe.evento.repositories.UserGroupSubscriptionRepository;
import com.paysafe.evento.repositories.UserMailSubscriptionRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.paysafe.evento.mail.EmailServiceImpl;
import com.paysafe.evento.mail.MailContentBuilder;
import com.paysafe.evento.repositories.MailTemplatesRepository;
import com.paysafe.evento.repositories.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class MailServiceTests {
    @Mock(name = "userRepository")
    private UserRepository userRepositoryMock;
    @Autowired
    @InjectMocks
    private EmailServiceImpl emailServiceIml;
    @Mock(name = "mailTemplateRepository")
    private MailTemplatesRepository mailTemplatesRepository;
    @Mock(name = "mailContentBuilder")
    private MailContentBuilder mailContentBuilder;
    @MockBean
    private UserMailSubscriptionRepository userMailSubscriptionRepository;
    @MockBean
    private UserGroupSubscriptionRepository userGroupSubscriptionRepository;

    private GreenMail smtpServer;

    @Before
    public void setUp() throws Exception {
        this.smtpServer = new GreenMail(ServerSetupTest.SMTP);
        this.smtpServer.start();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
        this.smtpServer.stop();
    }

    @Test
    public void test() {
        GreenMailUtil.sendTextEmailTest("to@localhost.com", "from@localhost.com", "some subject", "some body");                                                                                // here
        assertEquals("some body", GreenMailUtil.getBody(this.smtpServer.getReceivedMessages()[0]));
    }

    @Test
    @WithMockUser
    public void shouldSendMail() throws Exception {
        Event event = new Event();
        event.setLink("http://www.google.com");
        event.setDescription("Test message content");
        event.setName("jPrime");
        String type = "NEW";
        User userMock = Mockito.mock(User.class);
        Group groupMock = Mockito.mock(Group.class);
        List<Group> groupList  = new ArrayList<>();
        groupList.add(groupMock);
        event.setAssignedGroups(groupList);
        when(userMock.getEmail()).thenReturn("test@email.com");
        when(userMock.isEnabledNotifications()).thenReturn(true);
        when(this.userRepositoryMock.findAll()).thenReturn(new ArrayList<User>() {{
            add(userMock);
        }});
        MailTemplates mailTemplateMock = Mockito.mock(MailTemplates.class);
        when(mailTemplateMock.getSubject()).thenReturn("New event");
        when(this.mailTemplatesRepository.findByType("NEW")).thenReturn(mailTemplateMock);
        String content = "<span>" + event.getDescription() + "</span>";
        when(this.mailContentBuilder.build(event.getName(), event.getDescription(), event.getLink(), type, userMock.getName(), event.getId())).thenReturn(content);
        UserMailSubscription userMailSubscription = new UserMailSubscription();
        userMailSubscription.setEnabled(true);
        List<UserGroupSubscription> userGroupSubscriptionList = new ArrayList<>();
        UserGroupSubscription userGroupSubscription = Mockito.mock(UserGroupSubscription.class);

        userGroupSubscriptionList.add(userGroupSubscription);
        when(userGroupSubscription.getSubscribedGroup()).thenReturn(groupMock);
        when(userMailSubscriptionRepository.findFirstByMailTemplatesAndUser(mailTemplateMock, userMock))
                .thenReturn(userMailSubscription);
        when(userGroupSubscriptionRepository.findAllByUserAndNotifiedAndSubscribed(userMock, true,true))
                .thenReturn(userGroupSubscriptionList);
        when(userGroupSubscription.getSubscribedGroup().getName()).thenReturn(event.getName());
        this.emailServiceIml.sendMailToAllUsers(type, event);
        Mockito.verify(this.mailContentBuilder).build(event.getName(), event.getDescription(), event.getLink(), type, userMock.getName(), event.getId());

        assertReceivedMessageContains(content);
    }

    private void assertReceivedMessageContains(String expected) throws IOException, MessagingException {
        MimeMessage[] receivedMessages = this.smtpServer.getReceivedMessages();
        assertEquals(1, receivedMessages.length);
        String content = (String) receivedMessages[0].getContent();
        assertTrue(content.contains(expected));
    }
}
