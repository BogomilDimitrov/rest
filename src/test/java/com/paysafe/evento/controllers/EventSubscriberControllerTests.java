package com.paysafe.evento.controllers;

import com.paysafe.evento.entites.*;
import com.paysafe.evento.repositories.EventRepository;
import com.paysafe.evento.repositories.EventSubscriberRepository;
import com.paysafe.evento.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EventSubscriberController.class)
@RunWith(SpringRunner.class)
public class EventSubscriberControllerTests {

    private static final String USER_ID = "1";
    private static final String ALL_SUBSCRIBERS_PATH = "/subscribe/all/{userId}";
    private static final String SUBSCRIBE_PATH = "/subscribe/";
    private static final String EXIST_USER_ID = "asd-123";
    private static final String NOT_EXIST_USER_ID = "fsdf";
    private static final String EXIST_USER_EMAIL = "jason@abv.bg";
    private static final String NOT_EXIST_USER_EMAIL = "bob@sty.bz";
    private static final String EXIST_EVENT_ID = "666";
    private static final String NOT_EXIST_EVENT_ID = "55";
    private static final String VALID_SUBSCRIBE_OPTION = "Going";
    private static final String INVALID_SUBSCRIBE_OPTION = "FALSE";
    private User testUser;
    private TestingAuthenticationToken testingAuthenticationToken;

    @Autowired
    private MockMvc mvc;
    @Autowired
    private WebApplicationContext wac;
    @MockBean
    private EventRepository eventRepository;
    @MockBean
    private EventSubscriberRepository eventSubscriberRepository;
    @MockBean
    private UserController userController;
    @MockBean
    private UserRepository userRepository;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(wac).build();
        this.testUser = new User();
        this.testUser.setId(USER_ID);
        this.testingAuthenticationToken = new TestingAuthenticationToken(this.testUser, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
    }

    @Test
    @WithMockUser
    public void givenNotExistingUserMailGetBadRequestResponse() throws Exception {
        when(this.userRepository.findUserById(USER_ID)).thenReturn(null);
        Event mockEvent = Mockito.mock(Event.class);
        when(this.eventRepository.findById(EXIST_EVENT_ID)).thenReturn(mockEvent);
        when(this.eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(null, mockEvent))
                .thenReturn(null);
        mvc.perform(MockMvcRequestBuilders.patch(SUBSCRIBE_PATH)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("eventId", EXIST_EVENT_ID)
                .param("subscriptionType", VALID_SUBSCRIBE_OPTION)
                .param("userEmail", NOT_EXIST_USER_EMAIL))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenNotExistingEventIdGetBadRequestResponse() throws Exception {
        when(this.userRepository.findUserById(USER_ID)).thenReturn(this.testUser);
        when(this.eventRepository.findById(NOT_EXIST_EVENT_ID)).thenReturn(null);
        when(this.eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(this.testUser, null))
                .thenReturn(null);
        mvc.perform(MockMvcRequestBuilders.patch(SUBSCRIBE_PATH)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("eventId", NOT_EXIST_EVENT_ID)
                .param("subscriptionType", VALID_SUBSCRIBE_OPTION))
                .andExpect(status().isBadRequest());
    }

    @WithMockUser(username = EXIST_USER_EMAIL)
    @Test
    public void givenInvalidSubscriptionTypeGetBadRequestResponse() throws Exception {
        Event mockEvent = mock(Event.class);
        when(userRepository.findUserById(USER_ID)).thenReturn(this.testUser);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(mockEvent);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(this.testUser, mockEvent))
                .thenReturn(null);
        mvc.perform(MockMvcRequestBuilders.patch(SUBSCRIBE_PATH)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("eventId", EXIST_EVENT_ID)
                .param("subscriptionType", INVALID_SUBSCRIBE_OPTION))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void givenValidFormParametersWhenCreatingNewEventSubscriberGetOkResponse() throws Exception {
        when(userRepository.findUserById(USER_ID)).thenReturn(this.testUser);
        Event mockEvent = new Event();
        mockEvent.setId(EXIST_EVENT_ID);
        mockEvent.setStatus(EventStatus.OPEN_FOR_ENROLLMENT);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(mockEvent);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(this.testUser, mockEvent))
                .thenReturn(null);
        mvc.perform(MockMvcRequestBuilders.patch(SUBSCRIBE_PATH)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("eventId", EXIST_EVENT_ID)
                .param("subscriptionType", VALID_SUBSCRIBE_OPTION)
                .param("userEmail", EXIST_USER_EMAIL))
                .andExpect(status().isOk());
    }


    @Test
    public void givenNotExistentUserIdReturnBadRequestForAllUserSubscribers() throws Exception {
        when(userController.findById(NOT_EXIST_USER_ID)).thenReturn(null);
        mvc.perform(MockMvcRequestBuilders.get(ALL_SUBSCRIBERS_PATH, NOT_EXIST_EVENT_ID)
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void givenExistingUserWithNoEventSubscribersReturnMapWithSizeZero() throws Exception {
        when(userController.findById(EXIST_USER_ID)).thenReturn(this.testUser);
        this.testUser.setSubscribedEvents(new ArrayList<>());
        mvc.perform(MockMvcRequestBuilders.get(ALL_SUBSCRIBERS_PATH, EXIST_USER_ID))
                .andExpect(status().isOk())
                .andExpect(content().string("{}"));
    }

    @Test
    public void givenExistingUserWithOneEventSubscriberReturnMapWithEventIdAndSubscriptionType() throws Exception {
        when(userController.findById(EXIST_USER_ID)).thenReturn(this.testUser);
        EventSubscriber mockEventSubscriber = mock(EventSubscriber.class);
        mockEventSubscriber.setSubscriptionType(SubscriptionType.valueOf(VALID_SUBSCRIBE_OPTION.toUpperCase()));
        Event mockEvent = mock(Event.class);
        mockEvent.setId(EXIST_EVENT_ID);
        when(mockEventSubscriber.getEvent()).thenReturn(mockEvent);
        when(mockEventSubscriber.getEvent().getId()).thenReturn(EXIST_EVENT_ID);
        when(mockEventSubscriber.getSubscriptionType())
                .thenReturn(SubscriptionType.valueOf(VALID_SUBSCRIBE_OPTION.toUpperCase()));
        List<EventSubscriber> mockCollection = new ArrayList<>();
        mockCollection.add(mockEventSubscriber);
        this.testUser.setSubscribedEvents(mockCollection);
        mvc.perform(MockMvcRequestBuilders.get(ALL_SUBSCRIBERS_PATH, EXIST_USER_ID))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"666\":\"GOING\"}"));
    }

    @Test
    public void givenNoEventIdGetBadRequestWhenPostingFeedbackGetBadRequest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.patch(SUBSCRIBE_PATH)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("nothing", ""))
                .andExpect(status().isBadRequest());
    }
}
