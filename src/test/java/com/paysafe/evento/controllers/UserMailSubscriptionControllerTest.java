package com.paysafe.evento.controllers;

import com.paysafe.evento.EventoApplication;
import com.paysafe.evento.entites.MailTemplates;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.entites.UserMailSubscription;
import com.paysafe.evento.repositories.MailTemplatesRepository;
import com.paysafe.evento.repositories.UserMailSubscriptionRepository;
import com.paysafe.evento.repositories.UserRepository;
import net.minidev.json.JSONArray;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EventoApplication.class)
@WebAppConfiguration
public class UserMailSubscriptionControllerTest {
    private static final String PATH = "";
    private MockMvc mockMvc;
    private User testUser;
    private ObjectMapper objectMapper;
    private MailTemplates testMailTemplate;
    private UserMailSubscription testSubscription;

    @InjectMocks
    private UserMailSubscriptionController userMailSubscriptionController;
    @MockBean
    private MailTemplatesRepository mailTemplatesRepository;
    @MockBean
    private UserMailSubscriptionRepository userMailSubscriptionRepository;
    @MockBean
    private UserController userController;
    @MockBean
    private UserRepository userRepository;

    @Before
    public void setUp() {
        this.objectMapper = new ObjectMapper();
        this.testUser = new User();
        this.testMailTemplate = new MailTemplates();
        this.testMailTemplate.setFilePath("path/to/template");
        this.testMailTemplate.setSubject("Test");
        this.testSubscription = new UserMailSubscription();
        this.testSubscription.setUser(this.testUser);
        this.testSubscription.setMailTemplates(this.testMailTemplate);

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(this.userMailSubscriptionController)
                .build();
    }

    @Test
    @WithMockUser
    public void whenGettingNotificationSettingShouldReturnCorrect() throws Exception {
        String path = PATH + "/userNotifications";
        this.testMailTemplate.setType("CLOSED");
        this.testSubscription.setEnabled(true);
        testUser.setId("1");

        List<UserMailSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(this.testSubscription);
        testUser.setUserMailSubscriptions(subscriptions);
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(testUser,null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        Mockito.when(userRepository.findUserById("1")).thenReturn(testUser);
        Mockito.when(this.userMailSubscriptionRepository.findAllByUser(this.testUser))
                .thenReturn(subscriptions);

        this.mockMvc
                .perform(MockMvcRequestBuilders.get(path)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser
    public void whenEditingNotificationSettingsShouldReturnCorrect() throws Exception {
        String path = PATH + "/editNotifications";
        testUser.setId("1");
        List<UserMailSubscription> mockList = new ArrayList<>();
        UserMailSubscription mockUserMailSubscription = Mockito.mock(UserMailSubscription.class);
        mockList.add(mockUserMailSubscription);
        UserMailSubscription testSubscription = new UserMailSubscription();
        Mockito.when(mockUserMailSubscription.getUser()).thenReturn(this.testUser);
        Mockito.when(mockUserMailSubscription.getMailTemplates()).thenReturn(this.testMailTemplate);
        Mockito.when(mockUserMailSubscription.isEnabled()).thenReturn(false);

        Mockito.when(this.userRepository.findUserById(testUser.getId())).thenReturn(this.testUser);
        Mockito.when(this.mailTemplatesRepository.findByType("OPEN"))
                .thenReturn(this.testMailTemplate);
        Mockito.when(this.userMailSubscriptionRepository.findFirstByMailTemplatesAndUser(this.testMailTemplate, this.testUser))
                .thenReturn(testSubscription);

        this.mockMvc
                .perform(MockMvcRequestBuilders.patch(path)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JSONArray.toJSONString(mockList)))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}