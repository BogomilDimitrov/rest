package com.paysafe.evento.controllers;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.Group;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.entites.UserGroupSubscription;
import com.paysafe.evento.repositories.UserGroupSubscriptionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(UserGroupSubscriptionController.class)
@RunWith(SpringRunner.class)
public class UserGroupSubscriptionControllerTests {

    private static final String PATH = "/user-groups";

    @InjectMocks
    private UserGroupSubscriptionController userGroupSubscriptionController;

    @MockBean
    private UserGroupSubscriptionRepository userGroupSubscriptionRepository;

    private MockMvc mvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(userGroupSubscriptionController).build();


    }

    @Test
    public void givenValidUserWithZeroSubscriptionsToGroupsReturnEmptyCollection() throws Exception {
        User mockUser = Mockito.mock(User.class);
        when(userGroupSubscriptionRepository.findAllByUser(mockUser))
                .thenReturn(new ArrayList<>());
        this.mvc.perform(MockMvcRequestBuilders.get(PATH)
                .sessionAttr("currentUser", mockUser))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    public void givenOneUserGroupSubscriptionSaveItWithTheCurrentUser() throws Exception {
        User mockUser = Mockito.mock(User.class);
        UserGroupSubscription mockSub = mock(UserGroupSubscription.class);
        mockSub.setUser(mockUser);
        List<UserGroupSubscription> mockList = new ArrayList<>();
        mockList.add(mockSub);
        when(userGroupSubscriptionRepository.findAllByUser(mockUser))
                .thenReturn(mockList);
        verify(mockSub, only()).setUser(mockUser);
        mvc.perform(MockMvcRequestBuilders.get(PATH)
                .sessionAttr("currentUser", mockUser))
                .andExpect(status().isOk());
    }

    @Test
    public void whenGettingEventsFromUserSubscriptionGroups_ShouldReturnTheEvents() throws Exception {
        String path = PATH + "/inMyGroups";
        User mockUser = Mockito.mock(User.class);
        UserGroupSubscription mockSubs = new UserGroupSubscription();
        Group mockGroup = new Group();
        Event jenkinsEvent = new Event();
        jenkinsEvent.setName("Jenkins Event");
        mockGroup.setName("DevOps");
        mockSubs.setUser(mockUser);
        mockSubs.setSubscribedGroup(mockGroup);

        List<UserGroupSubscription> mockList = new ArrayList<>();
        mockList.add(mockSubs);

        Mockito.when(this.userGroupSubscriptionRepository.findAllByUser(mockUser))
                .thenReturn(mockList);

        this.mvc
                .perform(MockMvcRequestBuilders.get(path)
                        .sessionAttr("currentUser", mockUser))
                .andExpect(status().isOk());

    }

}
