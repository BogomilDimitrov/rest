package com.paysafe.evento.controllers;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventSubscriber;
import com.paysafe.evento.entites.SubscriptionType;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.repositories.EventRepository;
import com.paysafe.evento.repositories.EventSubscriberRepository;
import com.paysafe.evento.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@WebMvcTest(ReportController.class)
@RunWith(SpringRunner.class)
public class ReportControllerTests {

    private static final String USERNAME = "Bob";
    private static final String EMAIL = "mymail@hotmail.com";
    private static final SubscriptionType SUB_GOING = SubscriptionType.GOING;

    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private MockMvc mockMvc;
    @Mock
    private ReportController reportController;
    @MockBean
    private EventRepository eventRepository;
    @MockBean
    private EventSubscriberRepository eventSubscriberRepository;
    @MockBean
    private UserRepository userRepository;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void givenExistingEventId_ShouldReturnOkAndMediaTypeOctetStream() throws Exception {
        Event testEvent = new Event();
        testEvent.setName("testName");
        when(this.eventRepository.findOne("1"))
                .thenReturn(testEvent);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/reports/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_OCTET_STREAM));
    }

    @Test
    public void givenExistingUserWhenGeneratingReport_ShouldReturnOk() throws Exception {
        User user = new User();
        user.setId("1");
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        when(this.userRepository.findOne("1"))
                .thenReturn(user);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/reports/user/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_OCTET_STREAM));
    }

    @Test
    public void givenNoSubscribedUsersForEventReturnEmptyList() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Event mockEvent = mock(Event.class);
        List<EventSubscriber> eventSubscriberList = new ArrayList<>();
        when(mockEvent.getSubscribedUsers()).thenReturn(eventSubscriberList);
        Method method = ReportController.class.getDeclaredMethod("getSubscribedUsers", Event.class);
        method.setAccessible(true);
        List<ReportController.SubscribedUser> list = (List<ReportController.SubscribedUser>) method.invoke(this.reportController, mockEvent);
        assertEquals(0, list.size());
    }

    @Test
    public void givenOneSubscriberGoingForEventThenReturnListWithValidSubscribedUser() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Event mockEvent = mock(Event.class);
        List<EventSubscriber> eventSubscriberList = new ArrayList<>();
        when(mockEvent.getSubscribedUsers()).thenReturn(eventSubscriberList);
        EventSubscriber mockEventSubscriber = mock(EventSubscriber.class);
        User mockUser = mock(User.class);
        when(mockEventSubscriber.getSubscriber()).thenReturn(mockUser);
        when(mockUser.getName()).thenReturn(USERNAME);
        when(mockUser.getEmail()).thenReturn(EMAIL);
        when(mockEventSubscriber.getSubscriptionType()).thenReturn(SUB_GOING);
        eventSubscriberList.add(mockEventSubscriber);
        Method method = ReportController.class.getDeclaredMethod("getSubscribedUsers", Event.class);
        method.setAccessible(true);
        List<ReportController.SubscribedUser> result = (List<ReportController.SubscribedUser>) method.invoke(this.reportController, mockEvent);
        assertEquals(1, result.size());
        assertEquals(USERNAME, result.get(0).getName());
        assertEquals(EMAIL, result.get(0).getEmail());
        assertEquals(SUB_GOING.toString(), result.get(0).getSubscription_type());
    }
}
