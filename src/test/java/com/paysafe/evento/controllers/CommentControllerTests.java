package com.paysafe.evento.controllers;

import com.paysafe.evento.entites.Comment;
import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.mail.EmailServiceImpl;
import com.paysafe.evento.repositories.*;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

@WebMvcTest(CommentController.class)
@RunWith(SpringRunner.class)
public class CommentControllerTests {

	private static final String COMMENT_PATH = "/comments";

	@Autowired
	private MockMvc commentController;
	@Autowired
	private WebApplicationContext wac;
	@MockBean
	private UserController userController;
	@MockBean
	private UserRepository userRepository;
	@MockBean
	private CommentRepository commentRepository;
	@MockBean
	private EventRepository eventRepository;
	@MockBean
	private EmailServiceImpl emailService;
	@MockBean
	private GroupRepository groupRepository;
	@MockBean
	private EventStatusChangeRepository eventStatusChangeRepository;
	

	@Before
	public void setUp() {
		commentController = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	@WithMockUser
	public void postingAValidCommentToAnExistingEvent_shouldReturnOk() throws Exception {
		Event event = new Event();
		Map<String, String> mockParams = new HashMap<>();
		mockParams.put("eventId", "1");
		mockParams.put("commentContent", "Great");
		mockParams.put("timeOfPosting", "2017-11-11 11:11:11.0");
		User mockUser 	= new User();
		when(userRepository.findUserById("1")).thenReturn(mockUser);
		when(eventRepository.findOne(mockParams.get("eventId"))).thenReturn(event);
		TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(mockUser,null);
		SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
		commentController.perform(MockMvcRequestBuilders.post(COMMENT_PATH)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
				.content("eventId=" + mockParams.get("eventId") + "&commentContent=" + mockParams.get("commentContent")
						+ "&timeOfPosting=" + mockParams.get("timeOfPosting")))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@WithMockUser
	public void postingAnEmptyComment_shouldReturnBadRequest() throws Exception {
		Event event = new Event();
		Map<String, String> mockParams = new HashMap<>();
		mockParams.put("eventId", "1");
		mockParams.put("commentContent", "");
		mockParams.put("timeOfPosting", "2017-11-11 11:11:11.0");

		when(eventRepository.findOne(mockParams.get("eventId"))).thenReturn(event);

		commentController
				.perform(MockMvcRequestBuilders.post(COMMENT_PATH)
						.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.content("&commentContent=" + mockParams.get("commentContent")))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@WithMockUser
	@Test
	public void postingAnEmptyCommentToANonExistingEvent_shouldReturnBadRequest() throws Exception {
		Map<String, String> mockParams = new HashMap<>();
		mockParams.put("eventId", "1");
		mockParams.put("commentContent", "Great event!");
		mockParams.put("timeOfPosting", "2017-11-11 11:11:11.0");

		when(eventRepository.findOne(mockParams.get("eventId"))).thenReturn(null);

		commentController.perform(MockMvcRequestBuilders.post(COMMENT_PATH)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
				.content("eventId=" + mockParams.get("eventId") + "&commentContent=" + mockParams.get("commentContent")
						+ "&timeOfPosting=" + mockParams.get("timeOfPosting")))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@WithMockUser
	@Test
	public void gettingCommentsOfEvent_shouldReturnStatusOk() throws Exception {
		Event event = new Event();
		Comment comment = new Comment();
		comment.setContent("Great!");
		
		Map<String, String> mockParams = new HashMap<>();
		mockParams.put("eventId", "1");
				when(this.eventRepository.findById(mockParams.get("eventId"))).thenReturn(event);
		when(this.commentRepository.findAllByEventOrderByDateOfCreationDesc(event))
		.thenReturn(Lists.newArrayList(comment));
		
		commentController.perform(MockMvcRequestBuilders.get(COMMENT_PATH + "/" + mockParams.get("eventId"))
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
				.content("eventId=" + mockParams.get("eventId")))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
}
