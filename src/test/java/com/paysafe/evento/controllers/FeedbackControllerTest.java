package com.paysafe.evento.controllers;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.entites.EventSubscriber;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.repositories.EventRepository;
import com.paysafe.evento.repositories.EventSubscriberRepository;
import com.paysafe.evento.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FeedbackController.class)
@RunWith(SpringRunner.class)
public class FeedbackControllerTest {

	private static final String FEEDBACK_PATH = "/feedback";
	private static final String FEEDBACK_INFORMATION = FEEDBACK_PATH + "/information/{eventId}";
	private static final String FEEDBACK_CHECK_AVAILABILITY = FEEDBACK_PATH + "/{eventId}/feedbackAvailable";
	private static final String EXIST_EVENT_ID = "666";
	private static final String NOT_EXIST_EVENT_ID = "55";
	private static final String VALID_FEEDBACK_COMMENT = "Good Event";
	private static final String VALID_FEEDBACK_RATING = "3";
	private static final String INVALID_FEEDBACK_RATING = "0";

	@Autowired
    private WebApplicationContext wac;
    @Autowired
	private MockMvc mvc;

	@MockBean
	private EventRepository eventRepository;
	@MockBean
	private EventSubscriberRepository eventSubscriberRepository;
	@MockBean
	private UserRepository userRepository;
	@MockBean
	private UserController userController;

	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

    @Test
    public void givenInvalidRatingGetBadRequestWhenPostingFeedbackGetBadRequest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.patch(FEEDBACK_PATH)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("eventId", NOT_EXIST_EVENT_ID)
                .param("comment", VALID_FEEDBACK_COMMENT)
                .param("rating", INVALID_FEEDBACK_RATING))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenEventNotClosedWhenGivingFeedbackGetBadRequest() throws Exception {
        Event event = mock(Event.class);
        when(event.getStatus()).thenReturn(EventStatus.OPEN_FOR_ENROLLMENT);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(event);
        mvc.perform(MockMvcRequestBuilders.patch(FEEDBACK_PATH)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("eventId", EXIST_EVENT_ID)
                .param("comment", VALID_FEEDBACK_COMMENT)
                .param("rating", VALID_FEEDBACK_COMMENT))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenCommentAlreadyGivenForEventGetBadRequest() throws Exception {
        Event event = mock(Event.class);
        when(event.getStatus()).thenReturn(EventStatus.OPEN_FOR_ENROLLMENT);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(event);
        User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);;
        when(userRepository.findOne("1")).thenReturn(user);
        EventSubscriber mockEventSubscriber = mock(EventSubscriber.class);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, event))
                .thenReturn(mockEventSubscriber);
        when(mockEventSubscriber.getFeedbackComment()).thenReturn(VALID_FEEDBACK_COMMENT);
        mvc.perform(MockMvcRequestBuilders.patch(FEEDBACK_PATH)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("eventId", EXIST_EVENT_ID)
                .param("comment", VALID_FEEDBACK_COMMENT)
                .param("rating", VALID_FEEDBACK_RATING))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void givenValidParametersThenReturnOk() throws Exception {
        Event event = mock(Event.class);
        when(event.getStatus()).thenReturn(EventStatus.CLOSED);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(event);

        User user = new User();
        user.setId("1");
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        when(userRepository.findOne("1")).thenReturn(user);

        EventSubscriber testSubscriber = new EventSubscriber();
        testSubscriber.setFeedbackComment(null);
        testSubscriber.setFeedbackRating(0);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, event))
                .thenReturn(testSubscriber);

        System.out.println("User: " + user);
        System.out.println("Event: " + event);

        mvc.perform(MockMvcRequestBuilders.patch(FEEDBACK_PATH)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("eventId", EXIST_EVENT_ID)
                .param("comment", VALID_FEEDBACK_COMMENT)
                .param("rating", VALID_FEEDBACK_RATING))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser
    public void givenInvalidEventIdReturnEmptyMap() throws Exception {
    	User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);;
        when(userRepository.findOne("1")).thenReturn(user);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(null);
        mvc.perform(MockMvcRequestBuilders.get(FEEDBACK_INFORMATION, EXIST_EVENT_ID))
                .andExpect(status().isOk())
                .andExpect(content().string("{}"));
    }

    @Test
    @WithMockUser
    public void whenUserIsNotSubscribedReturnCommentUnavailableTrueAndEventRating() throws Exception {
    	User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);;
        when(userRepository.findOne("1")).thenReturn(user);
        Event mockEvent = Mockito.mock(Event.class);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(mockEvent);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, mockEvent))
                .thenReturn(null);
        when(eventSubscriberRepository.findEventSubscribersByEvent(mockEvent)).thenReturn(new ArrayList<EventSubscriber>());
        mvc.perform(MockMvcRequestBuilders.get(FEEDBACK_INFORMATION, EXIST_EVENT_ID))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"hasFeedback\":\"false\",\"rating\":\"0\"}"));
    }

    @Test
    @WithMockUser
    public void whenUserIsSubscribedReturnCommentUnavailableFalseWhenCommentIsNotGivenAndEventRating() throws Exception {
    	User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);;
        when(userRepository.findOne("1")).thenReturn(user);
        Event mockEvent = Mockito.mock(Event.class);
        EventSubscriber mockEventSubscriber = mock(EventSubscriber.class);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(mockEvent);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, mockEvent))
                .thenReturn(mockEventSubscriber);
        when(eventSubscriberRepository.findEventSubscribersByEvent(mockEvent)).thenReturn(new ArrayList<EventSubscriber>());
        mvc.perform(MockMvcRequestBuilders.get(FEEDBACK_INFORMATION, EXIST_EVENT_ID))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"hasFeedback\":\"false\",\"rating\":\"0\"}"));
    }

    @Test
    @WithMockUser
    public void whenUserIsSubscribedReturnCommentUnavailableFalseAndCalculatingTotalEventRating() throws Exception {
    	User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);;
        when(userRepository.findOne("1")).thenReturn(user);
        List<EventSubscriber> mockEventSubscriberList = new ArrayList<>();
        Event mockEvent = Mockito.mock(Event.class);
        EventSubscriber mockEventSubscriber = mock(EventSubscriber.class);
        EventSubscriber mockEs = mock(EventSubscriber.class);
        when(mockEs.getFeedbackRating()).thenReturn(3);
        when(mockEventSubscriber.getFeedbackRating()).thenReturn(5);
        mockEventSubscriberList.add(mockEventSubscriber);
        mockEventSubscriberList.add(mockEs);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(mockEvent);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, mockEvent))
                .thenReturn(mockEventSubscriber);

        when(eventSubscriberRepository.findEventSubscribersByEvent(mockEvent)).thenReturn(mockEventSubscriberList);
        mvc.perform(MockMvcRequestBuilders.get(FEEDBACK_INFORMATION, EXIST_EVENT_ID))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"hasFeedback\":\"false\",\"rating\":\"4\"}"));
    }

    @Test
    @WithMockUser
    public void whenUserIsSubscribedButHadGivenFeedBackShouldReturnCommentUnavailableTrueAndCalculatingTotalEventRating() throws Exception {
    	User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        when(userRepository.findOne("1")).thenReturn(user);
        List<EventSubscriber> mockEventSubscriberList = new ArrayList<>();
        Event mockEvent = Mockito.mock(Event.class);
        EventSubscriber mockEventSubscriber = mock(EventSubscriber.class);
        EventSubscriber mockEs = mock(EventSubscriber.class);
        when(mockEs.getFeedbackRating()).thenReturn(3);
        when(mockEventSubscriber.getFeedbackRating()).thenReturn(5);
        mockEventSubscriberList.add(mockEventSubscriber);
        mockEventSubscriberList.add(mockEs);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(mockEvent);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, mockEvent))
                .thenReturn(mockEventSubscriber);
        when(mockEventSubscriber.getFeedbackComment()).thenReturn(VALID_FEEDBACK_COMMENT);
        when(eventSubscriberRepository.findEventSubscribersByEvent(mockEvent)).thenReturn(mockEventSubscriberList);
        mvc.perform(MockMvcRequestBuilders.get(FEEDBACK_INFORMATION, EXIST_EVENT_ID))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"hasFeedback\":\"false\",\"rating\":\"4\"}"));
    }
    
    @Test
    @WithMockUser
    public void whenInvalidEventIdIsProvidedShouldReturnNotFound() throws Exception {
    	User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        when(userRepository.findOne("1")).thenReturn(user);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(null);
        mvc.perform(MockMvcRequestBuilders.get(FEEDBACK_CHECK_AVAILABILITY, EXIST_EVENT_ID))
                .andExpect(status().isNotFound());
    }
    
    @Test
    @WithMockUser
    public void whenTheUserIsNotSubscribedShouldReturnOkAndFeedbackAvailableFalse() throws Exception {
    	User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        Event mockEvent = Mockito.mock(Event.class);
        EventSubscriber mockEventSubscriber = mock(EventSubscriber.class);
        when(mockEventSubscriber.getFeedbackRating()).thenReturn(5);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(mockEvent);
        when(userRepository.findOne("1")).thenReturn(user);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, mockEvent))
                .thenReturn(null);
        mvc.perform(MockMvcRequestBuilders.get(FEEDBACK_CHECK_AVAILABILITY, EXIST_EVENT_ID))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"feedbackAvailable\":false}"));;
    }
    
    @Test
    @WithMockUser
    public void whenTheEventIsNotClosedShouldReturnOkAndFeedbackAvailableFalse() throws Exception {
    	User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        Event mockEvent = new Event();
        mockEvent.setId(EXIST_EVENT_ID);
        mockEvent.setStatus(EventStatus.OPEN_FOR_ENROLLMENT);
        EventSubscriber mockEventSubscriber = mock(EventSubscriber.class);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(mockEvent);
        when(userRepository.findOne("1")).thenReturn(user);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, mockEvent))
                .thenReturn(mockEventSubscriber);
        mvc.perform(MockMvcRequestBuilders.get(FEEDBACK_CHECK_AVAILABILITY, EXIST_EVENT_ID))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"feedbackAvailable\":false}"));;
    }
    
    @Test
    @WithMockUser
    public void whenTheUserHadGivenFeedbackShouldReturnOkAndFeedbackAvailableFalse() throws Exception {
    	User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        Event mockEvent = new Event();
        mockEvent.setId(EXIST_EVENT_ID);
        mockEvent.setStatus(EventStatus.OPEN_FOR_ENROLLMENT);
        EventSubscriber mockEventSubscriber = mock(EventSubscriber.class);
        when(mockEventSubscriber.getFeedbackRating()).thenReturn(5);
        when(mockEventSubscriber.getFeedbackComment()).thenReturn("Test");
        when(userRepository.findOne("1")).thenReturn(user);
        when(eventRepository.findById(EXIST_EVENT_ID)).thenReturn(mockEvent);
        when(eventSubscriberRepository.findEventSubscriberBySubscriberAndEvent(user, mockEvent))
                .thenReturn(mockEventSubscriber);
        mvc.perform(MockMvcRequestBuilders.get(FEEDBACK_CHECK_AVAILABILITY, EXIST_EVENT_ID))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"feedbackAvailable\":false}"));;
    }
}
