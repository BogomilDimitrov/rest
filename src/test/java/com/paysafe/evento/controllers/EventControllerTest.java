package com.paysafe.evento.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.paysafe.evento.repositories.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.entites.Group;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.entites.UserRole;
import com.paysafe.evento.mail.EmailServiceImpl;
import com.paysafe.evento.search.EventSpecificationBuilder;
import com.paysafe.evento.search.SearchOperation;

@WebMvcTest(EventController.class)
@RunWith(SpringRunner.class)
public class EventControllerTest {

    private static final String EVENTS_PATH = "/events";

    @Autowired
    private MockMvc mockEventController;
    private ObjectMapper mapper;
    @Autowired
    private WebApplicationContext wac;
    @MockBean
    private EventRepository mockEventRepository;
    @MockBean
    private EmailServiceImpl emailService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private UserController userController;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private EventStatusChangeRepository eventStatusChangeRepository;
    @MockBean
    private EventSubscriberRepository eventSubscriberRepository;
    @MockBean
    private UserGroupSubscriptionRepository userGroupSubscriptionRepository;

    private Event mockEvent = new Event();
    private Group mockGroup = new Group("ExistingGroup");
    private Collection<Group> mockGroups = new HashSet<>();
    private User user;

    @Before
    public void setUp() {
    	mockGroups.add(mockGroup);
        mapper = new ObjectMapper();
        mockGroups.add(mockGroup);
        mapper = new ObjectMapper();
        mockEventController = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    @WithMockUser
    public void saveEvent_shouldReturnOk() throws Exception {
        mockEvent.setDate(new Timestamp(System.currentTimeMillis() + 1000));
        mockEvent.setDueDate(new Timestamp(System.currentTimeMillis() + 1000));
        User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        mockEvent.setAssignedGroups(this.mockGroups);
        String json = mapper.writeValueAsString(mockEvent);
        when(groupRepository.findByName(mockGroup.getName()))
                .thenReturn(this.mockGroup);
        when(userRepository.findUserById("1")).thenReturn(user);
        mockEventController.perform(post(EVENTS_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void givenNotExistingGroupWhenCreateEventShouldReturnError() throws Exception {
        mockEvent.setDate(new Timestamp(System.currentTimeMillis() + 1000));
        mockEvent.setDueDate(new Timestamp(System.currentTimeMillis() + 1000));
        mockEvent.setAssignedGroups(mockGroups);
        User user = new User();
        user.setId("1");
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user,null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        when(userRepository.findUserById("1")).thenReturn(user);
        String json = mapper.writeValueAsString(mockEvent);
        when(groupRepository.findByName(mockGroup.getName()))
                .thenReturn(null);
        mockEventController.perform(post(EVENTS_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void whenGettingOpenEventsSortedByDateThenShouldJSONWithEvents() throws Exception {
        this.mockEventController
                .perform(MockMvcRequestBuilders.get(EVENTS_PATH)
                        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    @WithMockUser
    public void changeStatus_shouldReturnOk() throws Exception {
        String path = EVENTS_PATH + "/changeStatus";

        Event event = new Event();
        event.setId("1");
        User user = new User();
        user.setId("1");
        user.setRole(UserRole.ADMIN);
        user.setEmail("test@gmail.com");
        event.setCreator(user);

        when(mockEventRepository.findOne(event.getId())).thenReturn(event);
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user,null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        when(userRepository.findUserById("1")).thenReturn(user);
        String json = "{\"eventId\": \"1\",\"statusType\": \"open_for_enrollment\",\"changeTime\": \"2017-11-11 11:11:11.0\",\"userId\": \"1\"}";

        this.mockEventController
                .perform(MockMvcRequestBuilders.patch(path)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser
    public void givenExistingEventWhenEditEventThenShouldReturnResponseOkWithEvent() throws Exception {
        String path = EVENTS_PATH + "/edit";
        Timestamp now = Timestamp.valueOf(LocalDateTime.now());
        User user = new User();
        user.setId("1");
        user.setRole(UserRole.ADMIN);
        user.setEmail("test@gmail.com");
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user,null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        mockEvent = new Event("Test Event", now, "description", null,
                "Sofia", EventStatus.OPEN_FOR_ENROLLMENT, now, null, now);
        when(userRepository.findUserById("1")).thenReturn(user);
        mockEvent.setAssignedGroups(mockGroups);
        mockEvent.setCreator(user);
        String json = mapper.writeValueAsString(mockEvent);


        when(mockEventRepository.findOne(mockEvent.getId())).thenReturn(mockEvent);

        when(groupRepository.findByName(this.mockGroup.getName())).thenReturn(mockGroup);
        mockEventController.perform(MockMvcRequestBuilders.patch(path)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser
    public void givenUserIsNotCreatorOrWhenEditEventThenShouldReturnResponseBadRequest() throws Exception {
        String path = EVENTS_PATH + "/edit";
        Timestamp now = Timestamp.valueOf(LocalDateTime.now());
        User user = new User();
        user.setId("1");
        user.setRole(UserRole.ADMIN);
        User mockCurrentUser = new User();
        User mockUserRepo = mock(User.class);
        when(mockUserRepo.getId()).thenReturn("1");
        mockCurrentUser.setEmail("currentTest@gmail.com");
        user.setEmail("test@gmail.com");
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user,null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        mockEvent = new Event("Test Event", now, "description", null,
                "Sofia", EventStatus.OPEN_FOR_ENROLLMENT, now, user, now);
        mockEvent.setAssignedGroups(mockGroups);
        String json = mapper.writeValueAsString(mockEvent);

        when(mockEventRepository.findOne(mockEvent.getId())).thenReturn(mockEvent);
        when(userRepository.findUserById("1")).thenReturn(mockCurrentUser);
        when(groupRepository.findByName(this.mockGroup.getName())).thenReturn(mockGroup);
        mockEventController.perform(MockMvcRequestBuilders.patch(path)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    @WithMockUser
    public void givenNotExistingGroupWhenEditEventThenShouldReturnResponseBadRequest() throws Exception {
        String path = EVENTS_PATH + "/edit";
        Timestamp now = Timestamp.valueOf(LocalDateTime.now());
        User mockUser = new User();
        mockUser.setId("1");
        mockUser.setRole(UserRole.ADMIN);
        mockUser.setEmail("test@gmail.com");
        User mockUserRepo = mock(User.class);
        when(mockUserRepo.getId()).thenReturn("1");
        mockEvent = new Event("Test Event", now, "description", null,
                "Sofia", EventStatus.OPEN_FOR_ENROLLMENT, now, mockUser, now);
        mockEvent.setAssignedGroups(mockGroups);
        String json = mapper.writeValueAsString(mockEvent);

        when(mockEventRepository.findOne(mockEvent.getId())).thenReturn(mockEvent);
        when(userRepository.findUserById(mockUserRepo.getId())).thenReturn(mockUser);
        when(groupRepository.findByName(this.mockGroup.getName())).thenReturn(null);
        mockEventController.perform(MockMvcRequestBuilders.patch(path)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void givenNotExistingEventWhenEditEventThenShouldReturnResponseBadRequest() throws Exception {
        String path = EVENTS_PATH + "/edit";
        Timestamp now = Timestamp.valueOf(LocalDateTime.now());
        User mockUser = new User();
        mockUser.setId("1");
        mockUser.setEmail("test@gmail.com");
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(mockUser,null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        when(userRepository.findUserById("1")).thenReturn(mockUser);
        mockEvent = new Event("Test Event", now, "description", null,
                "Sofia", EventStatus.OPEN_FOR_ENROLLMENT, now, mockUser, now);
        mockEvent.setAssignedGroups(mockGroups);
        String json = mapper.writeValueAsString(mockEvent);
        when(groupRepository.findByName(this.mockGroup.getName())).thenReturn(mockGroup);
        when(mockEventRepository.findOne(mockEvent.getId())).thenReturn(null);
        mockEventController.perform(MockMvcRequestBuilders.patch(path)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    @WithMockUser
    public void givenExistingEventNameWhenSearchingThenShouldReturnCorrect() throws Exception {
        Event mockEvent = Mockito.mock(Event.class);
        mockEvent.setName("Java");
        List<Event> events = new ArrayList<>();
        events.add(mockEvent);

        EventSpecificationBuilder eventSpecificationBuilder = new EventSpecificationBuilder();
        Specification specification = eventSpecificationBuilder
                .with("name", SearchOperation.EQUALS, mockEvent)
                .build();

        when(this.mockEventRepository.findAll(specification, new PageRequest(0, 10)))
                .thenReturn(new PageImpl<Event>(events, new PageRequest(0, 10), 10));

        ResultActions result = this.mockEventController.perform(get(EVENTS_PATH)
                .content("eventName=" + "Java"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser
    public void givenEventDateNotInTheFuture_ShouldReturnBadRequest() throws Exception {
        User user = new User();
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        mockEvent.setDate(new Timestamp(System.currentTimeMillis() - 5000));
        mockEvent.setDueDate(new Timestamp(System.currentTimeMillis() - 5000));
        mockEvent.setAssignedGroups(mockGroups);
        String json = mapper.writeValueAsString(mockEvent);
        mockEventController.perform(post(EVENTS_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void givenEventDueDateBeforeEventDate_ShouldReturnBadRequest() throws Exception {
        mockEvent.setDate(new Timestamp(System.currentTimeMillis() + 1000));
        mockEvent.setDueDate(new Timestamp(System.currentTimeMillis() + 1500));
        mockEvent.setAssignedGroups(mockGroups);
        String json = mapper.writeValueAsString(mockEvent);
        mockEventController.perform(post(EVENTS_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    public void givenEventDueDateBeforeEventDateWhenEditing_ShouldReturnBadRequest() throws Exception {
        mockEvent.setDate(new Timestamp(System.currentTimeMillis() + 1000));
        mockEvent.setDueDate(new Timestamp(System.currentTimeMillis() + 2000));
        mockEvent.setAssignedGroups(mockGroups);
        String json = mapper.writeValueAsString(mockEvent);
        mockEventController.perform(patch(EVENTS_PATH + "/edit")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isBadRequest());
    }
}