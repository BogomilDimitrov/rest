package com.paysafe.evento.search;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.paysafe.evento.entites.Event;
import com.paysafe.evento.entites.EventStatus;
import com.paysafe.evento.entites.EventSubscriber;
import com.paysafe.evento.entites.SubscriptionType;
import com.paysafe.evento.entites.User;
import com.paysafe.evento.entites.UserRole;
import com.paysafe.evento.repositories.EventRepository;
import com.paysafe.evento.repositories.EventSubscriberRepository;
import com.paysafe.evento.repositories.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class SearchTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private EventSubscriberRepository eventSubscriberRepository;

    private Event eventJava;
    private Event eventPerl;
    private User user;
    private EventSpecificationBuilder builder;


    @Before
    public void init() {
        this.user = new User();
        this.user.setEmail("user@mail.bg");
        this.user.setName("Stoyo Stoyo");
        this.user.setRole(UserRole.USER);

        this.eventJava = new Event();
        this.eventPerl = new Event();
        this.eventJava.setName("Java");
        this.eventJava.setCreator(user);
        this.eventJava.setDate(Timestamp.valueOf("2030-09-27 01:20:00"));
        this.eventJava.setDueDate(Timestamp.valueOf("2030-09-29 01:20:00"));
        this.eventJava.setLocation("Sofia Bulgaria");
        this.eventJava.setStatus(EventStatus.OPEN_FOR_ENROLLMENT);
        this.eventJava.setHavingPicture(false);

        this.eventPerl.setName("Perl");
        this.eventPerl.setCreator(user);
        this.eventPerl.setDate(Timestamp.valueOf("2030-09-27 01:20:00"));
        this.eventPerl.setDueDate(Timestamp.valueOf("2030-09-29 01:20:00"));
        this.eventPerl.setLocation("Sofia Bulgaria");
        this.eventPerl.setStatus(EventStatus.OPEN_FOR_ENROLLMENT);
        this.eventPerl.setHavingPicture(false);
        this.eventRepository.save(this.eventPerl);
        this.userRepository.save(this.user);
        EventSubscriber subscribeToEvent = new EventSubscriber();
        subscribeToEvent.setEvent(eventPerl);
        subscribeToEvent.setSubscriber(user);
        subscribeToEvent.setId("1");
        subscribeToEvent.setSubscriptionType(SubscriptionType.GOING);
        this.eventSubscriberRepository.save(subscribeToEvent);

        this.user.setSubscribedEvents(new ArrayList<EventSubscriber>() {{
            add(subscribeToEvent);
        }});
        this.eventPerl.setSubscribedUsers(new ArrayList<EventSubscriber>() {{
            add(subscribeToEvent);
        }});
        this.eventRepository.save(this.eventPerl);
        this.userRepository.save(this.user);
        this.eventRepository.save(this.eventJava);

        this.builder = new EventSpecificationBuilder();
    }

    @Test
    public void givenExistingEventName_whenGettingListOfEvents_thenCorrect() {
        EventSpecification specification = new EventSpecification(
                new SearchCriteria("name", SearchOperation.EQUALS, "Java"));

        Page<Event> events = this.eventRepository.findAll(Specifications
                .where(specification), new PageRequest(0, 10));
        assertThat(this.eventJava).isIn(events);
    }

    @Test
    public void givenPartialEventName_whenGettingListOfEvents_thenCorrect() {
        Specification<Event> specifications = this.builder
                .with("name", SearchOperation.EQUALS, "Jav")
                .build();

        Page<Event> events = this.eventRepository
                .findAll(specifications, new PageRequest(0, 10));
        assertThat(this.eventJava).isIn(events);
    }

    @Test
    public void currentUserEvents_shouldContainOnlyUserSubscribedToAndCreatedByTheUser() throws Exception {
        Specification<Event> specification = this.builder
                .with("eventSubscribers", SearchOperation.MY_EVENTS, this.user)
                .build();

        this.eventJava.setSubscribedUsers(new ArrayList<>());
        User eventCreator = new User();
        eventCreator.setEmail("anotherUserMail@gmail.com");
        eventCreator.setRole(UserRole.USER);
        eventCreator.setName("anotherUser");
        eventCreator.setEnabledNotifications(true);
        this.userRepository.save(eventCreator);
        this.eventJava.setCreator(eventCreator);
        this.eventRepository.save(eventJava);
        this.eventPerl.setCreator(eventCreator);
        this.eventRepository.save(eventPerl);

        Page<Event> events = this.eventRepository
                .findAll(specification, new PageRequest(0, 20));

        assertThat(this.eventPerl).isIn(events);
        assertThat(this.eventJava).isNotIn(events);
    }

}
